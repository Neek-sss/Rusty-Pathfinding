use crate::glue::{
    graphic::{initialize_camera, initialize_map_graphics},
    state::LevelMapState,
};
use amethyst::{input::is_key_down, prelude::*, renderer::VirtualKeyCode};
use rusty_pathfinding::prelude::*;

pub struct FullMapState {
    pub passage_matrix_number: usize,
}

impl SimpleState for FullMapState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        initialize_map_graphics(data.world, self.passage_matrix_number, None);
        initialize_camera(data.world);
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_all();
    }

    fn on_pause(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_all();
    }

    fn on_resume(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        initialize_map_graphics(data.world, self.passage_matrix_number, None);
        initialize_camera(data.world);
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: StateEvent,
    ) -> SimpleTrans {
        let rmap = data.world.read_resource::<RecursiveMap<[usize; 3], u8>>();
        if let StateEvent::Window(event) = &event {
            if is_key_down(&event, VirtualKeyCode::Left) {
                let num_passage = rmap.passage_maps.len();
                let passage_matrix_number = if self.passage_matrix_number > 0 {
                    self.passage_matrix_number - 1
                } else {
                    num_passage - 1
                };
                return Trans::Switch(Box::new(FullMapState {
                    passage_matrix_number,
                }));
            }
            if is_key_down(&event, VirtualKeyCode::Right) {
                let num_passage = rmap.passage_maps.len();
                let passage_matrix_number = if self.passage_matrix_number < num_passage - 1 {
                    self.passage_matrix_number + 1
                } else {
                    0
                };
                return Trans::Switch(Box::new(FullMapState {
                    passage_matrix_number,
                }));
            }
            if is_key_down(&event, VirtualKeyCode::Down) {
                return Trans::Push(Box::new(LevelMapState {
                    passage_matrix_number: self.passage_matrix_number,
                    level: 0,
                }));
            }
        }

        // Escape isn't pressed, so we stay in this `State`.
        Trans::None
    }
}
