use crate::glue::{graphic::Sprites, state::FullMapState};
use amethyst::{
    assets::{AssetStorage, Loader},
    prelude::*,
    renderer::{PngFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture, TextureMetadata},
};
use rusty_pathfinding::{amethyst_system::prelude::*, prelude::*};
use std::{collections::HashMap, fs::File, io};

#[derive(Default)]
pub struct LoadState {
    pub passable_texture_path: String,
    pub passable_config_path: String,
    pub impassable_texture_path: String,
    pub impassable_config_path: String,
}

impl SimpleState for LoadState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.res.insert({
            let loader = data.world.res.fetch::<Loader>();
            let texture_storage = data.world.res.fetch::<AssetStorage<Texture>>();
            let sprite_sheet_store = data.world.res.fetch::<AssetStorage<SpriteSheet>>();

            let passable_texture = loader.load(
                self.passable_texture_path.clone(),
                PngFormat,
                TextureMetadata::srgb_scale(),
                (),
                &texture_storage,
            );

            let impassable_texture = loader.load(
                self.impassable_texture_path.clone(),
                PngFormat,
                TextureMetadata::srgb_scale(),
                (),
                &texture_storage,
            );

            Sprites {
                passable_sprite: SpriteRender {
                    sprite_sheet: loader.load(
                        self.passable_config_path.clone(),
                        SpriteSheetFormat,
                        passable_texture,
                        (),
                        &sprite_sheet_store,
                    ),
                    sprite_number: 0,
                },
                impassable_sprite: SpriteRender {
                    sprite_sheet: loader.load(
                        self.impassable_config_path.clone(),
                        SpriteSheetFormat,
                        impassable_texture,
                        (),
                        &sprite_sheet_store,
                    ),
                    sprite_number: 0,
                },
            }
        });
        load_map(data.world);
    }

    fn update(&mut self, _data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        Trans::Switch(Box::new(FullMapState {
            passage_matrix_number: 0,
        }))
    }
}

fn load_map(world: &mut World) {
    let rmap: RecursiveMap<[usize; 3], u8> = bincode::deserialize_from(io::BufReader::new(
        File::open("resources/map/rmap.bin").expect("File Error"),
    ))
    .unwrap();
    world.res.insert(rmap);
    let paths: HashMap<PassageMatrix<u8>, ([usize; 3], [usize; 3])> = bincode::deserialize_from(
        io::BufReader::new(File::open("resources/map/paths.bin").expect("File Error")),
    )
    .unwrap();
    for (passage_matrix, (from, to)) in paths {
        world
            .create_entity()
            .with(PathRequest {
                passage_matrix: passage_matrix.clone(),
                from,
                to,
            })
            .build();
    }
}
