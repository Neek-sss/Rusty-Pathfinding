use crate::glue::graphic::{initialize_camera, initialize_map_graphics};
use amethyst::{input::is_key_down, prelude::*, renderer::VirtualKeyCode};
use rusty_pathfinding::prelude::*;

pub struct LevelMapState {
    pub passage_matrix_number: usize,
    pub level: usize,
}

impl SimpleState for LevelMapState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        initialize_map_graphics(data.world, self.passage_matrix_number, Some(self.level));
        initialize_camera(data.world);
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_all();
    }

    fn on_pause(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_all();
    }

    fn on_resume(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        initialize_map_graphics(data.world, self.passage_matrix_number, Some(self.level));
        initialize_camera(data.world);
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: StateEvent,
    ) -> SimpleTrans {
        let rmap = data.world.read_resource::<RecursiveMap<[usize; 3], u8>>();
        if let StateEvent::Window(event) = &event {
            if is_key_down(&event, VirtualKeyCode::Down) {
                if self.level + 1 < rmap.recursive_size {
                    return Trans::Push(Box::new(LevelMapState {
                        passage_matrix_number: self.passage_matrix_number,
                        level: self.level + 1,
                    }));
                }
            }
            if is_key_down(&event, VirtualKeyCode::Up) {
                return Trans::Pop;
            }
        }

        // Escape isn't pressed, so we stay in this `State`.
        Trans::None
    }
}
