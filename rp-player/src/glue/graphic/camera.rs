use amethyst::{
    core::transform::Transform,
    prelude::*,
    renderer::{Camera, Projection},
};
use rusty_pathfinding::prelude::*;

pub fn initialize_camera(world: &mut World) {
    let rmap_size = world
        .read_resource::<RecursiveMap<[usize; 3], u8>>()
        .map_size;
    let mut transform = Transform::default();
    transform.set_z(1.0);
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            rmap_size[0] as f32,
            0.0,
            rmap_size[1] as f32,
        )))
        .with(transform)
        .build();
}
