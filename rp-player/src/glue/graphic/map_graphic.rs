use super::Sprites;
use amethyst::{core::transform::Transform, ecs::prelude::*};
use rusty_pathfinding::prelude::*;

pub fn initialize_map_graphics(
    world: &mut World,
    passage_matrix_number: usize,
    level_option: Option<usize>,
) {
    let (_map_size, vec): (_, Vec<_>) = {
        let rmap = world.read_resource::<RecursiveMap<[usize; 3], u8>>();
        if let Some(passage_matrix) = rmap.passage_maps.keys().nth(passage_matrix_number) {
//            let matrix = vec![(0, 255), (1, 124)];
//            if passage_matrix.clone() == (PassageMatrix{ matrix }) {
                if let Some(level) = level_option {
                    (
                        rmap.map_size,
                        rmap.map_size
                            .rmap_iter()
                            .map(|coord| {
                                (
                                    coord,
                                    rmap.get_passability(passage_matrix, &coord, Some(level)),
                                )
                            })
                            .collect(),
                    )
                } else {
                    (
                        rmap.map_size,
                        rmap.map_size
                            .rmap_iter()
                            .map(|coord| (coord, rmap.get_passability(passage_matrix, &coord, None)))
                            .collect(),
                    )
                }
//            } else {
//                (rmap.map_size, Vec::new())
//            }
        } else {
            (rmap.map_size, Vec::new())
        }
    };

    let (passable_sprite, impassable_sprite) = {
        let sprites = world.read_resource::<Sprites>();
        (
            sprites.passable_sprite.clone(),
            sprites.impassable_sprite.clone(),
        )
    };

    for (coord, passability_option) in vec {
        let mut transform = Transform::default();
        transform.set_x(coord[0] as f32 + 0.5);
        transform.set_y(coord[1] as f32 + 0.5);
        transform.set_z(0.0);
        transform.set_scale(1.0 / 33.0, 1.0 / 33.0, 1.0);
        if let Some(passability) = passability_option {
            if passability {
                world
                    .create_entity()
                    .with(passable_sprite.clone())
                    .with(transform)
                    .build();
            } else {
                world
                    .create_entity()
                    .with(impassable_sprite.clone())
                    .with(transform)
                    .build();
            }
        }
    }
}
