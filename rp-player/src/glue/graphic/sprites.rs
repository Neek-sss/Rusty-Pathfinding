use amethyst::renderer::SpriteRender;

pub struct Sprites {
    pub passable_sprite: SpriteRender,
    pub impassable_sprite: SpriteRender,
}
