use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage},
    utils::application_root_dir,
    LogLevelFilter, LoggerConfig,
};
use rusty_pathfinding::amethyst_system::prelude::*;
mod glue;
use self::glue::state::*;
use std::error::Error;

fn main() -> Result<(), Box<Error>> {
    let mut logger = LoggerConfig::default();
    logger.level_filter = LogLevelFilter::Warn;
    amethyst::start_logger(logger);

    let path = format!(
        "{}/resources/config/display_config.ron",
        application_root_dir()
    );
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new()),
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with(MapSystem::<[usize; 3], u8>::default(), "MapSystem", &[]);
    let mut game = Application::new(
        "./",
        LoadState {
            passable_texture_path: "resources/texture/passable.png".to_string(),
            passable_config_path: "resources/texture/passable.ron".to_string(),
            impassable_texture_path: "resources/texture/impassable.png".to_string(),
            impassable_config_path: "resources/texture/impassable.ron".to_string(),
            ..Default::default()
        },
        game_data,
    )?;

    game.run();

    Ok(())
}
