use proptest::prelude::*;
use rand::{prelude::*, Rng};
use rusty_pathfinding::prelude::*;

proptest! {
    #[test]
    fn edit_recursive_map(
        x_size in 2usize..10,
        y_size in 2usize..10,
        z_size in 2usize..10,
        base in 2usize..10,
        default_kind in any::<u64>(),
        num_edits in 0..500,
        random_string in ".{32}"
    ) {
        let mut random_seed = [0u8; 32];
        random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
        let mut rng = StdRng::from_seed(random_seed);

        let mut rmap = RecursiveMap::new(
            CoordinateSystemUSize3::new_size([x_size, y_size, z_size]),
            base,
            default_kind
        );

        let mut eq = EditQueue::new();
        for _ in 0..num_edits {
            eq.insert(
                CoordinateSystemUSize3::new_coord([
                    rng.gen_range(0..x_size),
                    rng.gen_range(0..y_size),
                    rng.gen_range(0..z_size)
                ]),
                rng.gen()
            );
        }
        eq.commit_no_compress(&mut rmap)?;
    }
}
