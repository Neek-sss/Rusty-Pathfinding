use proptest::prelude::*;
use rusty_pathfinding::prelude::*;

proptest! {
    #[test]
    fn new_recursive_map(
        x_size in 2usize..100000,
        y_size in 2usize..100000,
        z_size in 2usize..100000,
        base in 2usize..10,
        default_kind in any::<u64>(),
    ) {
        let _rmap = RecursiveMap::new(CoordinateSystemUSize3::new_size([x_size, y_size, z_size]), base, default_kind);
    }
}
