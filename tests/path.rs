use std::collections::HashMap;

use maplit::hashmap;

use rusty_pathfinding::prelude::*;
use rusty_pathfinding::testing::a_star;

// TODO: remake test for
//  0 0 F 0
//  0 1 1 0
//  0 0 T 0

#[test]
fn path_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut rmap = RecursiveMap::new(CoordinateSystemUSize3::new_size([4, 3, 1]), 2, 0);

    let edits = hashmap! {
        CoordinateSystemUSize3::new_coord([
            1,
            1,
            0,
        ]) => 1,
        CoordinateSystemUSize3::new_coord([
            2,
            1,
            0,
        ]) => 1,
        CoordinateSystemUSize3::new_coord([
            3,
            1,
            0,
        ]) => 1,
    };

    {
        let mut eq = EditQueue::new();
        eq.queue(edits.iter());
        eq.commit(&mut rmap)?;
    }

    let passage_matrix = PassageMatrix::new(&[(0.0, vec![0]), (255.0, vec![1])]);

    let mut paths = HashMap::new();
    paths
        .entry(passage_matrix.clone())
        .or_insert_with(std::collections::HashSet::new)
        .insert((
            CoordinateSystemUSize3::new_coord([2, 0, 0]),
            CoordinateSystemUSize3::new_coord([2, 2, 0]),
        ));
    paths
        .entry(passage_matrix.clone())
        .or_insert_with(std::collections::HashSet::new)
        .insert((
            CoordinateSystemUSize3::new_coord([2, 2, 0]),
            CoordinateSystemUSize3::new_coord([2, 0, 0]),
        ));

    println!("rmap_kind_string:\n{}", rmap.as_kind_string());
    println!(
        "rmap_recursive_kind_string:\n{}",
        rmap.as_recursive_kind_string()
    );
    println!(
        "rmap values:\n{}\n",
        paths.values().map(|v| v.len()).sum::<usize>()
    );

    let path_results = {
        let mut piq = PathInQueue::new();
        piq.queue(paths.iter());
        let mut poq = PathOutQueue::new();
        piq.create_empty_paths(&mut poq, &rmap);
        poq.commit(&mut rmap)
    };

    println!("rmap:\n{:?}\n", rmap);

    println!("coords & passages:");
    for (passage_matrix, _) in &paths {
        for coord in rmap.map_size.to_size().unwrap().rmap_iter() {
            let current_passage = passage_matrix
                .get_passage(rmap.get(&CoordinateSystemUSize3::new_coord(coord)).kind)?;
            println!("{:?}", coord);
            println!("{:?}", current_passage);
        }
    }
    println!();

    let a_star_paths = paths
        .clone()
        .into_iter()
        .map(|(passage_matrix, path_set)| {
            (
                passage_matrix.clone(),
                path_set
                    .into_iter()
                    .map(|(from, to)| {
                        (
                            (from.clone(), to.clone()),
                            a_star(&passage_matrix, &from, &to, &rmap),
                        )
                    })
                    .collect::<HashMap<_, _>>(),
            )
        })
        .collect::<HashMap<_, _>>();

    println!("A* Paths: {:?}", a_star_paths);

    for (passage_matrix, path_set) in path_results {
        for (coords, path_result) in path_set? {
            println!("Path: {:?}", coords);
            println!("Passage Matrix:");
            for (kind, passage) in &passage_matrix.matrix {
                let passability = PassageMatrix::<u8>::get_passability(*passage);
                println!("\t{}: {}", kind, passability);
            }

            println!("Passage Map:");
            println!("{}", rmap.as_passability_string(&passage_matrix));
            // println!("{:?}", path_result?.take(6).collect::<Vec<_>>());
            // panic!("Infinite loop when generating path");

            let rrffp_path = path_result?.collect::<Vec<_>>();

            let a_star_path = a_star_paths[&passage_matrix][&coords].clone();

            println!("{}", rmap.as_path_string(&coords.0, &rrffp_path));
            println!("{}", rmap.as_path_string(&coords.0, &a_star_path));

            println!("RRFFP:");
            rrffp_path.iter().cloned().for_each(|coord| {
                println!("\t{:?}", coord);
            });

            println!("A*:");
            a_star_path.iter().cloned().for_each(|coord| {
                println!("\t{:?}", coord);
            });

            assert_eq!(rrffp_path.len(), a_star_path.len());

            let mut last = coords.0;
            rrffp_path.iter().cloned().for_each(|coord| {
                assert_eq!(
                    last.to_coord().unwrap().distance(coord.to_coord().unwrap()),
                    1
                );
                last = coord;
            });

            println!();
        }
    }

    Ok(())
}

#[test]
fn simple_test() -> Result<(), Box<dyn std::error::Error>> {
    let mut rmap: RecursiveMap<CoordinateSystemUSize3, u8> =
        bincode::deserialize_from(std::io::BufReader::new(std::fs::File::open("rmap.bin")?))?;

    println!("rmap:\n{:?}\n", rmap);

    let paths: HashMap<
        PassageMatrix<u8>,
        std::collections::HashSet<(CoordinateSystemUSize3, CoordinateSystemUSize3)>,
    > = bincode::deserialize_from(std::io::BufReader::new(std::fs::File::open("paths.bin")?))?;

    println!("rmap_kind_string:\n{}", rmap.as_kind_string());
    println!(
        "rmap values:\n{}\n",
        paths.values().map(|v| v.len()).sum::<usize>()
    );

    let path_results = {
        let mut piq = PathInQueue::new();
        piq.queue(paths.iter());
        let mut poq = PathOutQueue::new();
        piq.create_empty_paths(&mut poq, &rmap);
        poq.commit(&mut rmap)
    };

    println!("coords & passages:");
    for (passage_matrix, _) in &paths {
        for coord in rmap.map_size.to_size().unwrap().rmap_iter() {
            let current_passage = passage_matrix
                .get_passage(rmap.get(&CoordinateSystemUSize3::new_coord(coord)).kind)?;
            println!("{:?}", coord);
            println!("{:?}", current_passage);
        }
    }
    println!();

    let a_star_paths = paths
        .clone()
        .into_iter()
        .map(|(passage_matrix, path_set)| {
            (
                passage_matrix.clone(),
                path_set
                    .into_iter()
                    .map(|(from, to)| {
                        (
                            (from.clone(), to.clone()),
                            a_star(&passage_matrix, &from, &to, &rmap),
                        )
                    })
                    .collect::<HashMap<_, _>>(),
            )
        })
        .collect::<HashMap<_, _>>();

    println!("A* Paths: {:?}", a_star_paths);

    for (passage_matrix, path_set) in path_results {
        for (coords, path_result) in path_set? {
            println!("Path: {:?}", coords);
            println!("Passage Matrix:");
            for (kind, passage) in &passage_matrix.matrix {
                let passability = PassageMatrix::<u8>::get_passability(*passage);
                println!("\t{}: {}", kind, passability);
            }

            println!("Passage Map:");
            println!("{}", rmap.as_passability_string(&passage_matrix));

            let rrffp_path = path_result?.collect::<Vec<_>>();

            let a_star_path = a_star_paths[&passage_matrix][&coords].clone();

            println!("{}", rmap.as_path_string(&coords.0, &rrffp_path));
            println!("{}", rmap.as_path_string(&coords.0, &a_star_path));

            println!("RRFFP:");
            rrffp_path.iter().cloned().for_each(|coord| {
                println!("\t{:?}", coord);
            });

            println!("A*:");
            a_star_path.iter().cloned().for_each(|coord| {
                println!("\t{:?}", coord);
            });

            assert_eq!(rrffp_path.len(), a_star_path.len());

            let mut last = coords.0;
            rrffp_path.iter().cloned().for_each(|coord| {
                assert_eq!(
                    last.to_coord().unwrap().distance(coord.to_coord().unwrap()),
                    1
                );
                last = coord;
            });

            println!();
        }
    }

    Ok(())
}

// proptest! {
//     #[test]
//     fn path_recursive_map(
//         x_size in 2usize..10,
//         y_size in 2usize..10,
//         z_size in 2usize..10,
//         base in 2usize..10,
//         default_kind in any::<u8>(),
//         num_edits in 0..500,
//         num_paths in 0..100,
//         num_passages in 0..3,
//         random_string in ".{32}"
//     ) {
//         let mut random_seed = [0u8; 32];
//         random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
//         let mut rng = StdRng::from_seed(random_seed);
//
//         let mut rmap = RecursiveMap::new(
//             CoordinateSystemUSize3::new_size([x_size, y_size, z_size]),
//             base,
//             default_kind
//         );
//
//         let mut edits = HashMap::new();
//         for _ in 0..num_edits {
//             edits.insert(
//                 CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]),
//                 rng.gen(),
//             );
//         }
//
//         let mut kinds: Vec<_> = edits.values().cloned().collect::<Vec<_>>();
//         kinds.sort();
//         kinds.dedup();
//
//         let mut paths = HashMap::new();
//         for _ in 0..num_passages {
//             let mut weights = Vec::new();
//             weights.push((rng.gen(), vec![default_kind]));
//             for kind in &kinds {
//                 weights.push((rng.gen(), vec![*kind]));
//             }
//             let passage_matrix = PassageMatrix::new(&weights);
//             for _ in 0..num_paths {
//                 let from = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]);
//                 let to = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]);
//                 paths
//                     .entry(passage_matrix.clone())
//                     .or_insert_with(std::collections::HashSet::new)
//                     .insert((from, to));
//             }
//         }
//
//         {
//             let mut eq = EditQueue::new();
//             eq.queue(edits.iter());
//             eq.commit(&mut rmap)?;
//         }
//
//         let path_results = {
//             let mut piq = PathInQueue::new();
//             piq.queue(paths.iter());
//             let mut poq = PathOutQueue::new();
//             piq.create_empty_paths(&mut poq, &rmap);
//             poq.commit(&mut rmap)
//         };
//
//         let a_star_paths = paths
//             .clone()
//             .into_iter()
//             .map(|(passage_matrix, path_set)| {
//                 (
//                     passage_matrix.clone(),
//                     path_set
//                         .into_iter()
//                         .map(|(from, to)| ((from.clone(), to.clone()), a_star(&passage_matrix, &from, &to, &rmap)))
//                         .collect::<HashMap<_, _>>(),
//                 )
//             })
//             .collect::<HashMap<_, _>>();
//
//         for (passage_matrix, path_set) in path_results {
//             for (coords, path_result) in path_set? {
//
//                 let rrffp_path = path_result?.collect::<Vec<_>>();
//                 let a_star_path = a_star_paths[&passage_matrix][&coords].clone();
//
//                 assert_eq!(rrffp_path.len(), a_star_path.len());
//
//                 let mut last = coords.0;
//                 rrffp_path.iter().cloned().for_each(|coord| {
//                     assert_eq!(
//                         last.to_coord().unwrap().distance(coord.to_coord().unwrap()),
//                         1
//                     );
//                     last = coord;
//                 });
//             }
//         }
//     }
//
//     #[test]
//     fn path_binary_recursive_map(
//         x_size in 2usize..10,
//         y_size in 2usize..10,
//         z_size in 2usize..10,
//         base in 2usize..10,
//         default_kind in 0u8..2,
//         num_edits in 0..500,
//         num_paths in 0..100,
//         num_passages in 0..10,
//         random_string in ".{32}"
//     ) {
//         let mut random_seed = [0u8; 32];
//         random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
//         let mut rng = StdRng::from_seed(random_seed);
//
//         let mut rmap = RecursiveMap::new(
//             CoordinateSystemUSize3::new_size([x_size, y_size, z_size]),
//             base,
//             default_kind
//         );
//
//         let mut edits = HashMap::new();
//         for _ in 0..num_edits {
//             edits.insert(
//                 CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]),
//                 rng.gen_range(0..2),
//             );
//         }
//
//         let mut kinds: Vec<_> = edits.values().cloned().collect::<Vec<_>>();
//         kinds.sort();
//         kinds.dedup();
//
//         let mut paths = HashMap::new();
//         for _ in 0..num_passages {
//             let mut weights = Vec::new();
//             weights.push((rng.gen(), vec![default_kind]));
//             for kind in &kinds {
//                 weights.push((rng.gen(), vec![*kind]));
//             }
//             let passage_matrix = PassageMatrix::new(&weights);
//             for _ in 0..num_paths {
//                 let from = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]);
//                 let to = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     rng.gen_range(0..z_size),
//                 ]);
//                 paths
//                     .entry(passage_matrix.clone())
//                     .or_insert_with(std::collections::HashSet::new)
//                     .insert((from, to));
//             }
//         }
//
//         {
//             let mut eq = EditQueue::new();
//             eq.queue(edits.iter());
//             eq.commit(&mut rmap)?;
//         }
//
//         let path_results = {
//             let mut piq = PathInQueue::new();
//             piq.queue(paths.iter());
//             let mut poq = PathOutQueue::new();
//             piq.create_empty_paths(&mut poq, &rmap);
//             poq.commit(&mut rmap)
//         };
//
//         let a_star_paths = paths
//             .clone()
//             .into_iter()
//             .map(|(passage_matrix, path_set)| {
//                 (
//                     passage_matrix.clone(),
//                     path_set
//                         .into_iter()
//                         .map(|(from, to)| ((from.clone(), to.clone()), a_star(&passage_matrix, &from, &to, &rmap)))
//                         .collect::<HashMap<_, _>>(),
//                 )
//             })
//             .collect::<HashMap<_, _>>();
//
//         for (passage_matrix, path_set) in path_results {
//             for (coords, path_result) in path_set? {
//
//                 let rrffp_path = path_result?.collect::<Vec<_>>();
//                 let a_star_path = a_star_paths[&passage_matrix][&coords].clone();
//
//                 assert_eq!(rrffp_path.len(), a_star_path.len());
//
//                 let mut last = coords.0;
//                 rrffp_path.iter().cloned().for_each(|coord| {
//                     assert_eq!(
//                         last.to_coord().unwrap().distance(coord.to_coord().unwrap()),
//                         1
//                     );
//                     last = coord;
//                 });
//             }
//         }
//     }
//
//     #[test]
//     fn path_single_level_binary_recursive_map(
//         x_size in 2usize..10,
//         y_size in 2usize..10,
//         base in 2usize..10,
//         default_kind in 0u8..2,
//         num_edits in 0..5,
//         num_paths in 0..100,
//         num_passages in 0..10,
//         random_string in ".{32}"
//     ) {
//         let mut random_seed = [0u8; 32];
//         random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
//         let mut rng = StdRng::from_seed(random_seed);
//
//         let mut rmap = RecursiveMap::new(
//             CoordinateSystemUSize3::new_size([x_size, y_size, 1]),
//             base,
//             default_kind
//         );
//
//         let mut edits = HashMap::new();
//         for _ in 0..num_edits {
//             edits.insert(
//                 CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     0,
//                 ]),
//                 rng.gen_range(0..2),
//             );
//         }
//
//         let mut kinds: Vec<_> = edits.values().cloned().collect::<Vec<_>>();
//         kinds.sort();
//         kinds.dedup();
//
//         let mut paths = HashMap::new();
//         for _ in 0..num_passages {
//             let mut weights = Vec::new();
//             weights.push((rng.gen(), vec![default_kind]));
//             for kind in &kinds {
//                 weights.push((rng.gen(), vec![*kind]));
//             }
//             let passage_matrix = PassageMatrix::new(&weights);
//             for _ in 0..num_paths {
//                 let from = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     0,
//                 ]);
//                 let to = CoordinateSystemUSize3::new_coord([
//                     rng.gen_range(0..x_size),
//                     rng.gen_range(0..y_size),
//                     0,
//                 ]);
//                 paths
//                     .entry(passage_matrix.clone())
//                     .or_insert_with(std::collections::HashSet::new)
//                     .insert((from, to));
//             }
//         }
//
//         {
//             let mut eq = EditQueue::new();
//             eq.queue(edits.iter());
//             eq.commit(&mut rmap)?;
//         }
//
//         bincode::serialize_into(
//             std::io::BufWriter::new(std::fs::File::create("paths.bin").expect("File Error")),
//             &paths,
//         )
//         .unwrap();
//
//         bincode::serialize_into(
//             std::io::BufWriter::new(std::fs::File::create("rmap.bin").expect("File Error")),
//             &rmap,
//         )
//         .unwrap();
//
//          let path_results = {
//             let mut piq = PathInQueue::new();
//             piq.queue(paths.iter());
//             let mut poq = PathOutQueue::new();
//             piq.create_empty_paths(&mut poq, &rmap);
//             poq.commit(&mut rmap)
//         };
//
//         let a_star_paths = paths
//             .clone()
//             .into_iter()
//             .map(|(passage_matrix, path_set)| {
//                 (
//                     passage_matrix.clone(),
//                     path_set
//                         .into_iter()
//                         .map(|(from, to)| ((from.clone(), to.clone()), a_star(&passage_matrix, &from, &to, &rmap)))
//                         .collect::<HashMap<_, _>>(),
//                 )
//             })
//             .collect::<HashMap<_, _>>();
//
//         for (passage_matrix, path_set) in path_results {
//             for (coords, path_result) in path_set? {
//
//                 let rrffp_path = path_result?.collect::<Vec<_>>();
//                 let a_star_path = a_star_paths[&passage_matrix][&coords].clone();
//
//                 assert_eq!(rrffp_path.len(), a_star_path.len());
//
//                 let mut last = coords.0;
//                 rrffp_path.iter().cloned().for_each(|coord| {
//                     assert_eq!(
//                         last.to_coord().unwrap().distance(coord.to_coord().unwrap()),
//                         1
//                     );
//                     last = coord;
//                 });
//             }
//         }
//     }
// }
