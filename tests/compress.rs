use std::collections::HashMap;

use proptest::prelude::*;
use rand::{prelude::*, Rng};

use rusty_pathfinding::prelude::*;

proptest! {
    #[test]
    fn compress_recursive_map(
        x_size in 2usize..10,
        y_size in 2usize..10,
        z_size in 2usize..10,
        base in 2usize..10,
        default_kind in any::<u64>(),
        num_edits in 0..500,
        random_string in ".{32}"
    ) {
        let mut random_seed = [0u8; 32];
        random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
        let mut rng = StdRng::from_seed(random_seed);

        let mut rmap = RecursiveMap::new(
            CoordinateSystemUSize3::new_size([x_size, y_size, z_size]),
            base,
            default_kind
        );

        let mut edits = HashMap::new();
        for _ in 0..num_edits {
            edits.insert(
                CoordinateSystemUSize3::new_coord([
                    rng.gen_range(0..x_size),
                    rng.gen_range(0..y_size),
                    rng.gen_range(0..z_size),
                ]),
                rng.gen(),
            );
        }

        {
            let mut eq = EditQueue::new();
            eq.queue(edits.iter());
            eq.commit(&mut rmap)?;
        }

        for coord in rmap.map_size.to_size().unwrap().rmap_iter() {
            assert_eq!(
                rmap.get(&CoordinateSystemUSize3::new_coord(coord)).kind,
                *edits.get(&CoordinateSystemUSize3::new_coord(coord)).unwrap_or(&default_kind),
                "Coord: {:?}",
                coord
            );
        }
    }

    #[test]
    fn compress_binary_recursive_map(
        x_size in 1usize..10,
        y_size in 1usize..10,
        z_size in 1usize..10,
        base in 2usize..10,
        default_kind in 0u8..2,
        num_edits in 0..500,
        random_string in ".{32}"
    ) {
        let mut random_seed = [0u8; 32];
        random_seed.clone_from_slice(random_string.as_bytes().split_at(32).0);
        let mut rng = StdRng::from_seed(random_seed);

        let mut rmap = RecursiveMap::new(
            CoordinateSystemUSize3::new_size([x_size, y_size, z_size]),
            base,
            default_kind
        );

        let mut edits = HashMap::new();
        for _ in 0..num_edits {
            edits.insert(
                CoordinateSystemUSize3::new_coord([
                    rng.gen_range(0..x_size),
                    rng.gen_range(0..y_size),
                    rng.gen_range(0..z_size),
                ]),
                rng.gen_range(0..2),
            );
        }

        {
            let mut eq = EditQueue::new();
            eq.queue(edits.iter());
            eq.commit(&mut rmap)?;
        }

        for coord in rmap.map_size.to_size().unwrap().rmap_iter() {
            assert_eq!(
                rmap.get(&CoordinateSystemUSize3::new_coord(coord)).kind,
                *edits.get(&CoordinateSystemUSize3::new_coord(coord)).unwrap_or(&default_kind),
                "Coord: {:?}",
                coord
            );
        }
    }
}
