#![deny(clippy::pedantic)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::type_complexity)]

use rand::prelude::*;
use rusty_pathfinding::prelude::*;
use std::{collections::HashMap, fs::File, io::BufWriter};
//x_size = 7, y_size = 5, base = 2, default_kind = 1, num_edits = 25, num_paths = 4, num_passages = 9, random_string = "\u{4fb95}\u{7f}\u{7f}%\u{43e63}\\\u{65941}%🕴\u{12acf}🕴\u{bf7ab}\u{feff}�/\u{1b}"
fn main() -> Result<(), RecursiveError> {
    let x_size = 7_usize;
    let y_size = 5;
    let base = 2;
    let default_kind = 1_u8;
    let num_edits = 25;
    let num_paths = 4;
    let num_passages = 9;
    let random_string = "abcde\\\u{65941}%🕴\u{12acf}🕴\u{bf7ab}\u{feff}�/\u{1b}";

    let mut random_seed = [0_u8; 16];
    random_seed.clone_from_slice(random_string.as_bytes().split_at(16).0);
    let mut rng = SmallRng::from_seed(random_seed);

    let mut rmap = RecursiveMap::new([x_size, y_size, 1], base, default_kind);

    let mut edits = HashMap::new();
    for _ in 0..num_edits {
        edits.insert(
            [
                rng.gen_range(0, x_size),
                rng.gen_range(0, y_size),
                0,
            ],
            rng.gen_range(0, 2),
        );
    }

    let mut kinds: Vec<_> = edits.values().cloned().collect::<Vec<_>>();
    kinds.sort();
    kinds.dedup();

    let mut paths = HashMap::new();
    for _ in 0..num_passages {
        let mut weights = Vec::new();
        weights.push((rng.gen(), vec![default_kind]));
        for kind in &kinds {
            weights.push((rng.gen(), vec![*kind]));
        }
        let passage_matrix = PassageMatrix::new(&weights);
        for _ in 0..num_paths {
            let from = [
                rng.gen_range(0, x_size),
                rng.gen_range(0, y_size),
                0,
            ];
            let to = [
                rng.gen_range(0, x_size),
                rng.gen_range(0, y_size),
                0,
            ];
            paths.insert(passage_matrix.clone(), (from, to));
        }
    }

    {
        let mut eq = EditQueue::new();
        eq.queue(edits.iter());
        eq.commit(&mut rmap)?;
    }

    bincode::serialize_into(
        BufWriter::new(File::create("paths.bin").expect("File Error")),
        &paths,
    )
    .unwrap();

    bincode::serialize_into(
        BufWriter::new(File::create("rmap.bin").expect("File Error")),
        &rmap,
    )
    .unwrap();

    let paths = {
        let mut piq = PathInQueue::new();
        let mut poq = PathOutQueue::new();
        piq.queue(paths.iter());
        piq.create_empty_paths(&mut poq, &rmap);
        for _ in 0..10 {
            poq.commit(&mut rmap);
        }
        poq.queues
    };

    for (_passage_matrix, path_set) in paths {
        for (coords, path) in path_set {
            let mut last = coords.0;
            path.for_each(|coord| {
                assert_eq!(last.distance(coord), 1);
                last = coord;
            })
        }
    }

    Ok(())
}
