#![deny(clippy::pedantic)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_precision_loss)]
#![allow(clippy::type_complexity)]

pub mod data_types;
pub mod prelude;
pub mod queue;
pub mod recursive_map;

//#[cfg(test)]
pub mod testing;

#[cfg(feature = "amethyst_system")]
pub mod amethyst_system;
