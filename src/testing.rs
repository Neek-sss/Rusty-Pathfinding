use std::collections::HashSet;

use maplit::hashmap;

use crate::{
    data_types::{CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveKind},
    recursive_map::RecursiveMap,
};

pub fn a_star<CS, K>(
    passage_matrix: &PassageMatrix<K>,
    from: &CS,
    to: &CS,
    rmap: &RecursiveMap<CS, K>,
) -> Vec<CS>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    println!("====A*: {:?}-{:?}====", from, to);
    let map_size = rmap.map_size.to_size().unwrap();

    let heuristic = |from: &CS, to: &CS| from.to_coord().unwrap().distance(to.to_coord().unwrap());

    let mut costs = hashmap! {*from => (0, heuristic(from, to))};

    let from_passable = PassageMatrix::<K>::get_passability(
        passage_matrix.get_passage(rmap.get(&from).kind).unwrap(),
    );
    let to_passable = PassageMatrix::<K>::get_passability(
        passage_matrix.get_passage(rmap.get(&to).kind).unwrap(),
    );

    if from_passable && to_passable {
        let mut visited_coords = HashSet::new();
        loop {
            let ((cost_from, _cost_to), neighbor_coords) = costs
                .clone()
                .into_iter()
                .filter(|(coord, _)| !visited_coords.contains(coord))
                .min_by(
                    |(_, (cost_to_1, cost_from_1)), (_, (cost_to_2, cost_from_2))| {
                        (*cost_from_1 + *cost_to_1).cmp(&(*cost_from_2 + *cost_to_2))
                    },
                )
                .and_then(|(coord, cost)| {
                    visited_coords.insert(coord);
                    Some((
                        cost,
                        coord
                            .to_coord()
                            .unwrap()
                            .neighbor_coords(map_size)
                            .into_iter()
                            .map(|c| CS::new_coord(c))
                            .collect::<Vec<_>>(),
                    ))
                })
                .unwrap();

            for neighbor_coord in neighbor_coords {
                let passable = PassageMatrix::<K>::get_passability(
                    passage_matrix
                        .get_passage(rmap.get(&neighbor_coord).kind)
                        .unwrap(),
                );
                if passable {
                    costs
                        .entry(neighbor_coord)
                        .or_insert_with(|| (cost_from + 1, heuristic(to, &neighbor_coord)));
                }
            }

            if costs.contains_key(to) || visited_coords.is_empty() {
                break;
            }
        }
    }

    let mut ret = Vec::new();
    if costs.contains_key(to) {
        ret.push(*to);
        while ret.last().unwrap() != from {
            let last_coord = ret.last().unwrap().clone();
            if let Some(min_neighbor) = last_coord
                .to_coord()
                .unwrap()
                .neighbor_coords(map_size)
                .into_iter()
                .filter_map(|c| {
                    let c_coord = CS::new_coord(c);
                    costs.get(&c_coord).and_then(|data| Some((c_coord, data)))
                })
                .min_by(|(_, (cost_from_1, _)), (_, (cost_from_2, _))| cost_from_1.cmp(cost_from_2))
                .map(|(coord, _)| coord)
            {
                ret.push(min_neighbor);
            }
        }
    }
    ret.into_iter().rev().skip(1).collect()
}
