use crate::{
    data_types::{CoordinateSystemTrait, PassageMatrix, RecursiveKind},
    queue::PathOutQueue,
    recursive_map::RecursiveMap,
};
use std::collections::{HashMap, HashSet};

#[derive(Debug, Default)]
pub struct PathInQueue<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub queues: HashMap<PassageMatrix<K>, HashSet<(CS, CS)>>,
}

impl<'a, CS, K> PathInQueue<CS, K>
where
    CS: 'a + CoordinateSystemTrait,
    K: 'a + RecursiveKind,
{
    pub fn new() -> Self {
        Self::default()
    }

    pub fn insert(&mut self, passage_matrix: &PassageMatrix<K>, from: CS, to: CS) {
        let queue = self
            .queues
            .entry(passage_matrix.clone())
            .or_insert_with(HashSet::new);
        queue.insert((from, to));
    }

    pub fn queue<I>(&mut self, iter: I)
    where
        I: Iterator<Item = (&'a PassageMatrix<K>, &'a HashSet<(CS, CS)>)>,
    {
        for (passage_matrix, coords) in iter {
            for &(from, to) in coords {
                self.insert(passage_matrix, from, to);
            }
        }
    }

    pub fn create_empty_paths(
        &mut self,
        path_out_queue: &mut PathOutQueue<CS, K>,
        rmap: &RecursiveMap<CS, K>,
    ) {
        for (passage_matrix, mut coords) in self.queues.drain() {
            for (from, to) in coords.drain() {
                let out_queues = path_out_queue
                    .queues
                    .entry(passage_matrix.clone())
                    .or_insert_with(HashMap::new);
                let _path = out_queues.entry((from, to)).or_insert(rmap.new_path(
                    &passage_matrix,
                    from,
                    to,
                ));
            }
        }
        self.queues = HashMap::new();
    }
}
