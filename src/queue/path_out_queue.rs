use std::collections::HashMap;

use crate::{
    data_types::{CoordinateSystemTrait, PassageMatrix, RecursiveError, RecursiveKind},
    recursive_map::{RecursiveMap, RecursivePath},
};

#[derive(Debug, Default)]
pub struct PathOutQueue<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub queues: HashMap<PassageMatrix<K>, HashMap<(CS, CS), RecursivePath<CS, K>>>,
}

impl<'a, CS, K> PathOutQueue<CS, K>
where
    CS: 'a + CoordinateSystemTrait,
    K: 'a + RecursiveKind,
{
    pub fn new() -> Self {
        Self::default()
    }

    pub fn commit_passage_only(
        &mut self,
        rmap: &mut RecursiveMap<CS, K>,
    ) -> Result<(), RecursiveError> {
        for (passage_matrix, _) in self.queues.iter() {
            rmap.update_passage(passage_matrix)?;
        }
        Ok(())
    }

    pub fn commit(
        &mut self,
        rmap: &mut RecursiveMap<CS, K>,
    ) -> HashMap<
        PassageMatrix<K>,
        Result<HashMap<(CS, CS), Result<RecursivePath<CS, K>, RecursiveError>>, RecursiveError>,
    > {
        self.queues
            .iter_mut()
            .map(|(passage_matrix, path_queues)| {
                (
                    passage_matrix.clone(),
                    match rmap.update_passage(&passage_matrix) {
                        Ok(_) => Ok(path_queues
                            .iter_mut()
                            .map(|(&coords, mut path)| {
                                println!("\ncoords: {coords:?}");
                                let mut flag = true;
                                let mut ret = Ok(path.clone());
                                //TODO: Remove this limiter
                                while flag {
                                    match rmap.refine_path(&mut path) {
                                        Ok(path_ret) => {
                                            flag = path_ret;
                                            ret = Ok(path.clone())
                                        }
                                        Err(error) => {
                                            flag = false;
                                            ret = Err(error)
                                        }
                                    }
                                }
                                (coords, ret)
                            })
                            .collect::<HashMap<_, _>>()),
                        Err(error) => Err(error),
                    },
                )
            })
            .collect::<HashMap<_, _>>()
    }
}
