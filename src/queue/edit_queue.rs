use crate::{
    data_types::{CoordinateSystemTrait, RecursiveError, RecursiveKind},
    recursive_map::RecursiveMap,
};
use std::collections::HashMap;

#[derive(Debug, Default)]
pub struct EditQueue<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    queue: HashMap<CS, K>,
}

impl<'a, CS, K> EditQueue<CS, K>
where
    CS: 'a + CoordinateSystemTrait,
    K: 'a + RecursiveKind,
{
    pub fn new() -> Self {
        Self::default()
    }

    pub fn insert(&mut self, coord: CS, kind: K) {
        self.queue.insert(coord, kind);
    }

    pub fn queue<I>(&mut self, iter: I)
    where
        I: Iterator<Item = (&'a CS, &'a K)>,
    {
        for (&coord, &kind) in iter {
            self.insert(coord, kind);
        }
    }

    pub fn commit_no_compress(
        mut self,
        rmap: &mut RecursiveMap<CS, K>,
    ) -> Result<(), RecursiveError> {
        for (coord, kind) in self.queue.drain() {
            rmap.edit(coord, kind);
        }
        Ok(())
    }

    pub fn commit(&mut self, rmap: &mut RecursiveMap<CS, K>) -> Result<(), RecursiveError> {
        for (coord, kind) in self.queue.drain() {
            rmap.edit(coord, kind);
        }
        rmap.compress()?;
        Ok(())
    }
}
