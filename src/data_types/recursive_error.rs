use std::{
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
};

#[derive(Debug)]
pub enum RecursiveError {
    Conversion,
    OutOfBounds,
    NeedUpdate,
    BottomOfStack,
    NotFound,
    NoData,
    Unknown,
}

impl Display for RecursiveError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            RecursiveError::Conversion => write!(f, "Cannot convert"),
            RecursiveError::OutOfBounds => write!(f, "Out of bounds"),
            RecursiveError::NeedUpdate => write!(f, "SubMaps need an update"),
            RecursiveError::BottomOfStack => write!(
                f,
                "Reached the bottom of the RegionIndicator stack too soon"
            ),
            RecursiveError::NotFound => write!(f, "Data could not be located"),
            RecursiveError::NoData => write!(f, "There is no data for this index"),
            RecursiveError::Unknown => write!(f, "Unknown error"),
        }
    }
}

impl Error for RecursiveError {
    fn description(&self) -> &str {
        match self {
            RecursiveError::Conversion => "Cannot convert",
            RecursiveError::OutOfBounds => "Out of bounds",
            RecursiveError::NeedUpdate => "SubMaps need an update",
            RecursiveError::BottomOfStack => {
                "Reached the bottom of the RegionIndicator stack too soon"
            }
            RecursiveError::NotFound => "Data could not be located",
            RecursiveError::NoData => "There is no data for this index",
            RecursiveError::Unknown => "Unknown error",
        }
    }
}
