use crate::data_types::{RecursiveError, RecursiveKind};
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Hash, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct PassageMatrix<K>
where
    K: RecursiveKind,
{
    pub matrix: Vec<(K, u8)>,
}

impl<K> PassageMatrix<K>
where
    K: RecursiveKind,
{
    pub fn new(tuples: &[(f64, Vec<K>)]) -> Self {
        let mut internal_tuples = tuples.to_owned();
        internal_tuples.sort_by(|a, b| a.partial_cmp(b).unwrap());

        let smallest = internal_tuples.iter().next().unwrap().0;
        let scale = internal_tuples.iter().last().unwrap().0 - smallest;

        let mut matrix_map = HashMap::new();
        for (passage, kinds) in internal_tuples {
            for kind in kinds {
                matrix_map.insert(kind, (((passage - smallest) / scale) * 255.0) as u8);
            }
        }
        let mut matrix_vec: Vec<_> = matrix_map.drain().collect();

        matrix_vec.sort_unstable_by(|item1, item2| item1.0.cmp(&item2.0));

        Self { matrix: matrix_vec }
    }

    pub fn get_passage(&self, kind: K) -> Result<u8, RecursiveError> {
        self.matrix
            .get(
                self.matrix
                    .binary_search_by(|item| item.0.cmp(&kind))
                    .map_err(|_| RecursiveError::Unknown)?,
            )
            .and_then(|tuple| Some(tuple.1))
            .ok_or(RecursiveError::Unknown)
    }

    pub fn get_passability(passage: u8) -> bool {
        passage < 128
    }
}
