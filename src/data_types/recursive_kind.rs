use std::{fmt::Debug, hash::Hash};

pub trait RecursiveKind:
    Copy + Eq + Debug + Default + Hash + Ord + PartialOrd + Send + Sync
{
}

impl RecursiveKind for u128 {}
impl RecursiveKind for u64 {}
impl RecursiveKind for u32 {}
impl RecursiveKind for u16 {}
impl RecursiveKind for u8 {}

impl RecursiveKind for i128 {}
impl RecursiveKind for i64 {}
impl RecursiveKind for i32 {}
impl RecursiveKind for i16 {}
impl RecursiveKind for i8 {}
