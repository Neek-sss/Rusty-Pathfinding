use crate::data_types::RecursiveCoordinate;
use std::{
    cmp::Eq,
    fmt::Debug,
    hash::Hash,
    marker::{Send, Sync},
};

pub trait RecursiveSize:
    Copy + Debug + PartialEq + Eq + Hash + PartialOrd + Default + Send + Sync
{
    fn as_size_stack(self, base: usize) -> Vec<Self>;
    fn rmap_iter(&self) -> RecursiveSizeIter<Self>
    where
        Self: RecursiveCoordinate;
}

pub struct RecursiveSizeIter<CS>
where
    CS: RecursiveCoordinate,
{
    coord: CS,
    size: CS,
}

impl Iterator for RecursiveSizeIter<[usize; 3]> {
    type Item = [usize; 3];
    fn next(&mut self) -> Option<Self::Item> {
        if self.coord[0] + 1 < self.size[0]
            && self.coord[1] < self.size[1]
            && self.coord[2] < self.size[2]
        {
            let c = self.coord;
            self.coord[0] += 1;
            Some(c)
        } else if self.coord[1] + 1 < self.size[1] && self.coord[2] < self.size[2] {
            let c = self.coord;
            self.coord[0] = 0;
            self.coord[1] += 1;
            Some(c)
        } else if self.coord[2] < self.size[2] {
            let c = self.coord;
            self.coord[0] = 0;
            self.coord[1] = 0;
            self.coord[2] += 1;
            Some(c)
        } else {
            None
        }
    }
}

impl RecursiveSize for [usize; 3] {
    fn as_size_stack(self, base: usize) -> Vec<Self> {
        let all_levels: Vec<Vec<_>> = self
            .iter()
            .map(|&size| {
                let mut levels = Vec::new();
                let mut current = 1;
                while current < size {
                    levels.push(current);
                    current *= base;
                }
                levels.push(current);
                levels.iter().cloned().rev().collect()
            })
            .collect();
        let mut i = 0;
        let mut ret = Vec::new();
        while i < all_levels[0].len() || i < all_levels[1].len() || i < all_levels[2].len() {
            ret.push([
                *all_levels[0].get(i).unwrap_or(&1),
                *all_levels[1].get(i).unwrap_or(&1),
                *all_levels[2].get(i).unwrap_or(&1),
            ]);
            i += 1;
        }
        ret = ret.iter().cloned().rev().collect();
        ret
    }

    fn rmap_iter(&self) -> RecursiveSizeIter<Self>
    where
        Self: RecursiveCoordinate,
    {
        RecursiveSizeIter {
            coord: [0; 3],
            size: *self,
        }
    }
}
