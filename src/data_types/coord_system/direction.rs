use std::{
    cmp::Eq,
    fmt::Debug,
    hash::Hash,
    marker::{Send, Sync},
};

use serde_derive::{Deserialize, Serialize};

#[derive(
    Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq, Hash, Default, Serialize, Deserialize,
)]
pub enum Direction2 {
    #[default]
    Left,
    Right,
    Down,
    Up,
}

#[derive(Copy, Clone, Debug, PartialOrd, Eq, PartialEq, Hash, Default, Serialize, Deserialize)]
pub enum Direction3 {
    Left,
    Right,
    Down,
    Up,
    Backward,
    Forward,
    #[default]
    None,
}

pub trait RecursiveDirection: Copy + Default + Debug + PartialEq + Eq + Hash + Send + Sync {
    fn directions() -> Vec<Self>;
    fn opposite(&self) -> Self;
}

impl RecursiveDirection for Direction2 {
    fn directions() -> Vec<Self> {
        vec![
            Direction2::Left,
            Direction2::Right,
            Direction2::Up,
            Direction2::Down,
        ]
    }

    fn opposite(&self) -> Self {
        match &self {
            Direction2::Left => Direction2::Right,
            Direction2::Right => Direction2::Left,
            Direction2::Down => Direction2::Up,
            Direction2::Up => Direction2::Down,
        }
    }
}

impl RecursiveDirection for Direction3 {
    fn directions() -> Vec<Self> {
        vec![
            Direction3::Left,
            Direction3::Right,
            Direction3::Up,
            Direction3::Down,
            Direction3::Backward,
            Direction3::Forward,
        ]
    }

    fn opposite(&self) -> Self {
        match &self {
            Direction3::Left => Direction3::Right,
            Direction3::Right => Direction3::Left,
            Direction3::Down => Direction3::Up,
            Direction3::Up => Direction3::Down,
            Direction3::Backward => Direction3::Forward,
            Direction3::Forward => Direction3::Backward,
            Direction3::None => Direction3::None,
        }
    }
}

impl RecursiveDirection for [usize; 3] {
    fn directions() -> Vec<Self> {
        vec![
            [1, 0, 0],
            [2, 0, 0],
            [0, 1, 0],
            [0, 2, 0],
            [0, 0, 1],
            [0, 0, 2],
        ]
    }

    fn opposite(&self) -> Self {
        match &self {
            [1, 0, 0] => [2, 0, 0],
            [2, 0, 0] => [1, 0, 0],
            [0, 1, 0] => [0, 2, 0],
            [0, 2, 0] => [0, 1, 0],
            [0, 0, 1] => [0, 0, 2],
            [0, 0, 2] => [0, 0, 1],
            _ => [0, 0, 0],
        }
    }
}
