use std::{
    fmt::Debug,
    hash::Hash,
    marker::{Send, Sync},
};

use crate::data_types::{Direction3, RecursiveDirection, RecursiveSize};

pub trait RecursiveCoordinate: Copy + Eq + Hash + Debug + Default + Send + Sync {
    type DirectionType: RecursiveDirection;
    type SizeType: RecursiveSize;

    fn as_coord_stack(self, base: usize, recursive_size: usize) -> Vec<Self>;
    fn next_level_coords(&self, base: usize, map_size: Self::SizeType) -> Vec<Self>;
    fn neighbor_coords(&self, map_size: Self::SizeType) -> Vec<Self>;
    fn neighbors(&self, other: Self) -> bool;
    fn directions(&self, towards: Self) -> Vec<Self::DirectionType>;
    fn add_direction(&self, direction: Self::DirectionType) -> Self;
    fn add(&self, other: Self) -> Self;
    fn on_edge(&self, base: usize, map_size: Self::SizeType) -> Vec<Self::DirectionType>;
    fn distance(&self, other: Self) -> usize;
    fn convert_upwards(&self, num_levels: usize, base: usize) -> Self;
    fn distance_to_edge(
        &self,
        direction: Self::DirectionType,
        base: usize,
        map_size: Self::SizeType,
    ) -> usize;
}

impl RecursiveCoordinate for [usize; 3] {
    type DirectionType = Direction3;
    type SizeType = [usize; 3];

    fn as_coord_stack(self, base: usize, recursive_size: usize) -> Vec<Self> {
        let mut sub_coord_stack = Vec::new();
        (0..recursive_size).fold(self, |coord, _| {
            sub_coord_stack.push(coord);
            [coord[0] / base, coord[1] / base, coord[2] / base]
        });
        sub_coord_stack.into_iter().rev().collect()
    }

    fn next_level_coords(&self, base: usize, map_size: Self::SizeType) -> Vec<Self> {
        [base; 3]
            .rmap_iter()
            .map(|sub_map_coord| {
                [
                    (self[0] * base) + sub_map_coord[0],
                    (self[1] * base) + sub_map_coord[1],
                    (self[2] * base) + sub_map_coord[2],
                ]
            })
            .filter(|sub_map_coord| {
                sub_map_coord[0] < map_size[0]
                    && sub_map_coord[1] < map_size[1]
                    && sub_map_coord[2] < map_size[2]
            })
            .collect()
    }

    fn neighbor_coords(&self, map_size: Self::SizeType) -> Vec<Self> {
        let mut ret = Vec::new();
        if self[0] > 0 {
            ret.push([self[0] - 1, self[1], self[2]]);
        }
        if self[1] > 0 {
            ret.push([self[0], self[1] - 1, self[2]]);
        }
        if self[2] > 0 {
            ret.push([self[0], self[1], self[2] - 1]);
        }
        if self[0] < map_size[0] - 1 {
            ret.push([self[0] + 1, self[1], self[2]]);
        }
        if self[1] < map_size[1] - 1 {
            ret.push([self[0], self[1] + 1, self[2]]);
        }
        if self[2] < map_size[2] - 1 {
            ret.push([self[0], self[1], self[2] + 1]);
        }
        ret
    }

    fn neighbors(&self, neighbor: Self) -> bool {
        (self[1] == neighbor[1] && self[2] == neighbor[2])
            || (self[0] == neighbor[0] && self[2] == neighbor[2])
            || (self[0] == neighbor[0] && self[1] == neighbor[1])
    }

    fn directions(&self, towards: Self) -> Vec<Self::DirectionType> {
        let mut ret = Vec::new();
        if self[0] > towards[0] {
            ret.push(Direction3::Left);
        }
        if self[0] < towards[0] {
            ret.push(Direction3::Right);
        }
        if self[1] > towards[1] {
            ret.push(Direction3::Up);
        }
        if self[1] < towards[1] {
            ret.push(Direction3::Down);
        }
        if self[2] > towards[2] {
            ret.push(Direction3::Backward);
        }
        if self[2] < towards[2] {
            ret.push(Direction3::Forward);
        }
        ret
    }

    fn add_direction(&self, direction: Self::DirectionType) -> Self {
        match direction {
            Direction3::Left => [self[0] - 1, self[1], self[2]],
            Direction3::Right => [self[0] + 1, self[1], self[2]],
            Direction3::Up => [self[0], self[1] - 1, self[2]],
            Direction3::Down => [self[0], self[1] + 1, self[2]],
            Direction3::Backward => [self[0], self[1], self[2] - 1],
            Direction3::Forward => [self[0], self[1], self[2] + 1],
            _ => *self,
        }
    }

    fn add(&self, other: Self) -> Self {
        [self[0] + other[0], self[1] + other[1], self[2] + other[2]]
    }

    fn on_edge(&self, base: usize, map_size: Self::SizeType) -> Vec<Self::DirectionType> {
        let mut ret = Vec::new();
        if self[0] > 0 && self[0] % base == 0 {
            ret.push(Direction3::Left);
        }
        if map_size[0] - 1 > self[0] && (self[0] + 1) % base == 0 {
            ret.push(Direction3::Right);
        }
        if self[1] > 0 && self[1] % base == 0 {
            ret.push(Direction3::Up);
        }
        if map_size[1] - 1 > self[1] && (self[1] + 1) % base == 0 {
            ret.push(Direction3::Down);
        }
        if self[2] > 0 && self[2] % base == 0 {
            ret.push(Direction3::Backward);
        }
        if map_size[2] - 1 > self[2] && (self[2] + 1) % base == 0 {
            ret.push(Direction3::Forward);
        }
        ret
    }

    fn distance(&self, other: Self) -> usize {
        let abs_sub = |first, second| {
            if first > second {
                first - second
            } else {
                second - first
            }
        };
        abs_sub(self[0], other[0]) + abs_sub(self[1], other[1]) + abs_sub(self[2], other[2])
    }

    fn convert_upwards(&self, num_levels: usize, base: usize) -> Self {
        let mut ret = *self;
        for _ in 0..num_levels {
            ret = [ret[0] / base, ret[1] / base, ret[2] / base];
        }
        ret
    }

    // TODO: Find a faster way
    fn distance_to_edge(
        &self,
        direction: Self::DirectionType,
        base: usize,
        map_size: Self::SizeType,
    ) -> usize {
        let mut coord = *self;
        let mut count = 0;
        while !coord.on_edge(base, map_size).contains(&direction) {
            coord = coord.add_direction(direction);
            count += 1;
        }
        count
    }
}
