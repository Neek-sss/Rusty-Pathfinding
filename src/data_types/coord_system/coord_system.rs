use crate::data_types::{Direction3, RecursiveCoordinate, RecursiveDirection, RecursiveSize};
use serde_derive::{Deserialize, Serialize};
use std::fmt::Display;
use std::{
    cmp::Eq,
    default::Default,
    error::Error,
    fmt::{Debug, Formatter, Result as FormatResult},
    hash::Hash,
    marker::{Send, Sync},
};

#[derive(Copy, Clone, Debug)]
pub enum CoordinateTraitError {
    IsCoord,
    IsDirection,
    IsSize,
}

impl Error for CoordinateTraitError {}

impl Display for CoordinateTraitError {
    fn fmt(&self, f: &mut Formatter) -> FormatResult {
        match &self {
            CoordinateTraitError::IsCoord => write!(f, "Is a coordinate"),
            CoordinateTraitError::IsDirection => write!(f, "Is a direction"),
            CoordinateTraitError::IsSize => write!(f, "Is a size"),
        }
    }
}

pub trait CoordinateSystemTrait: Copy + Clone + Default + Debug + Eq + Hash + Send + Sync {
    type CoordinateType: RecursiveCoordinate<
        DirectionType = Self::DirectionType,
        SizeType = Self::SizeType,
    >;
    type DirectionType: RecursiveDirection;
    type SizeType: RecursiveSize;
    fn new_coord(c: Self::CoordinateType) -> Self;
    fn default_coord() -> Self;
    fn to_coord(&self) -> Result<Self::CoordinateType, CoordinateTraitError>;
    fn new_direction(d: Self::DirectionType) -> Self;
    fn default_direction() -> Self;
    fn to_direction(&self) -> Result<Self::DirectionType, CoordinateTraitError>;
    fn new_size(s: Self::SizeType) -> Self;
    fn default_size() -> Self;
    fn to_size(&self) -> Result<Self::SizeType, CoordinateTraitError>;
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum CoordinateSystem<C, D, S>
where
    C: RecursiveCoordinate,
    D: RecursiveDirection,
    S: RecursiveSize,
{
    Coord(C),
    Direction(D),
    Size(S),
}

impl<C, D, S> Default for CoordinateSystem<C, D, S>
where
    C: RecursiveCoordinate,
    D: RecursiveDirection,
    S: RecursiveSize,
{
    fn default() -> Self {
        CoordinateSystem::Coord(C::default())
    }
}

impl<C, D, S> CoordinateSystemTrait for CoordinateSystem<C, D, S>
where
    C: RecursiveCoordinate<DirectionType = D, SizeType = S>,
    D: RecursiveDirection,
    S: RecursiveSize,
{
    type CoordinateType = C;
    type DirectionType = D;
    type SizeType = S;

    fn new_coord(c: Self::CoordinateType) -> Self {
        CoordinateSystem::Coord(c)
    }

    fn default_coord() -> Self {
        CoordinateSystem::Coord(Self::CoordinateType::default())
    }

    fn to_coord(&self) -> Result<Self::CoordinateType, CoordinateTraitError> {
        match self {
            CoordinateSystem::Coord(c) => Ok(*c),
            CoordinateSystem::Direction(_) => Err(CoordinateTraitError::IsDirection),
            CoordinateSystem::Size(_) => Err(CoordinateTraitError::IsSize),
        }
    }

    fn new_direction(d: Self::DirectionType) -> Self {
        CoordinateSystem::Direction(d)
    }

    fn default_direction() -> Self {
        CoordinateSystem::Direction(Self::DirectionType::default())
    }

    fn to_direction(&self) -> Result<Self::DirectionType, CoordinateTraitError> {
        match self {
            CoordinateSystem::Coord(_) => Err(CoordinateTraitError::IsCoord),
            CoordinateSystem::Direction(d) => Ok(*d),
            CoordinateSystem::Size(_) => Err(CoordinateTraitError::IsSize),
        }
    }

    fn new_size(s: Self::SizeType) -> Self {
        CoordinateSystem::Size(s)
    }

    fn default_size() -> Self {
        CoordinateSystem::Size(Self::SizeType::default())
    }

    fn to_size(&self) -> Result<Self::SizeType, CoordinateTraitError> {
        match self {
            CoordinateSystem::Coord(_) => Err(CoordinateTraitError::IsCoord),
            CoordinateSystem::Direction(_) => Err(CoordinateTraitError::IsDirection),
            CoordinateSystem::Size(s) => Ok(*s),
        }
    }
}

pub type CoordinateSystemUSize3 = CoordinateSystem<[usize; 3], Direction3, [usize; 3]>;
