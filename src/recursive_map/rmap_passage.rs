use std::collections::{HashMap, HashSet};

use crate::{
    data_types::{
        CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveDirection,
        RecursiveError, RecursiveKind,
    },
    recursive_map::{RecursiveMap, RegionIndex},
};
use crate::recursive_map::region_index::RegionIndicator;
use crate::recursive_map::rmap::RecursivePassageMap;

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn update_passage(
        &mut self,
        passage_matrix: &PassageMatrix<K>,
    ) -> Result<(), RecursiveError> {
        if !self.passage_maps.contains_key(passage_matrix) {
            self.passage_maps.insert(
                passage_matrix.clone(),
                (0..self.recursive_size).map(|_| HashMap::new()).collect(),
            );
        }

        if let Some(insert_stack) = self
            .passage_maps
            .get(passage_matrix)
            .and_then(|passage_map| {
                Some(self.down_stack(
                    &|level, coord, sub_map_option| {
                        if let Some(sub_map) = sub_map_option {
                            if let Some(passage_sub_map) = passage_map[level].get(&coord) {
                                sub_map.version != passage_sub_map.version
                            } else {
                                true
                            }
                        } else {
                            false
                        }
                    },
                    &|level, coord, sub_map_option| {
                        sub_map_option.is_some() && !passage_map[level].contains_key(&coord)
                    },
                ))
            })
        {
            let passage_map = self.passage_maps.get_mut(passage_matrix).unwrap();
            for (level, coords) in insert_stack.into_iter().enumerate().rev() {
                for coord in coords {
                    let sub_map = self.sub_maps[level][&coord];
                    passage_map[level].insert(
                        coord,
                        RecursivePassageMap {
                            version: sub_map.version.checked_add(1).unwrap_or_default(),
                            region_indices: Vec::new(),
                        },
                    );
                }
            }
        }

        if let Some(remove_stack) = self
            .passage_maps
            .get(passage_matrix)
            .and_then(|passage_map| {
                Some(self.down_stack(
                    &|level, coord, sub_map_option| {
                        if let Some(sub_map) = sub_map_option {
                            if let Some(passage_sub_map) = passage_map[level].get(&coord) {
                                sub_map.version != passage_sub_map.version
                            } else {
                                true
                            }
                        } else {
                            false
                        }
                    },
                    &|level, coord, sub_map_option| {
                        sub_map_option.is_none() && passage_map[level].contains_key(&coord)
                    },
                ))
            })
        {
            let passage_map = self.passage_maps.get_mut(passage_matrix).unwrap();
            for (level, coords) in remove_stack.into_iter().enumerate().rev() {
                for coord in coords {
                    passage_map[level].remove(&coord);
                }
            }
        }

        if let Some(up_stack) = self
            .passage_maps
            .get(passage_matrix)
            .and_then(|passage_map| {
                Some(self.up_stack(&|level, coord, sub_map_option| {
                    sub_map_option
                        .and_then(|sub_map| {
                            passage_map[level].get(&coord).and_then(|passage_sub_map| {
                                Some(sub_map.version != passage_sub_map.version)
                            })
                        })
                        .unwrap_or(false)
                }))
            })
        {
            for (level, coords) in up_stack.into_iter().enumerate().rev() {
                for coord in coords {
                    let regions = if self.empty_sub_map(level, coord) {
                        let mut region_index = RegionIndex::new(
                            passage_matrix
                                .get_passage(self.sub_maps[level][&coord].kind)
                                .unwrap_or(255),
                        );
                        if PassageMatrix::<K>::get_passability(region_index.passage) {
                            region_index.insert(
                                RegionIndicator::<CS>::default(),
                                &CS::DirectionType::directions()
                                    .iter()
                                    .map(|&direction| CS::new_direction(direction))
                                    .collect::<Vec<_>>(),
                            );
                        }
                        vec![region_index]
                    } else {
                        self.expanding_front_search(passage_matrix, coord, level)?
                    };

                    *self.passage_maps.get_mut(&passage_matrix).unwrap()[level]
                        .get_mut(&coord)
                        .unwrap() = RecursivePassageMap {
                        version: self.sub_maps[level][&coord].edit_version,
                        region_indices: regions,
                    };
                }
            }
        }

        Ok(())
    }

    pub fn expanding_front_search(
        &self,
        passage_matrix: &PassageMatrix<K>,
        coord: CS,
        level: usize,
    ) -> Result<Vec<RegionIndex<CS>>, RecursiveError> {
        let size = self.level_sizes[level + 1]
            .to_size()
            .expect("Internal size error");
        let default_passage = passage_matrix.get_passage(
            self.sub_maps[level]
                .get(&coord)
                .ok_or(RecursiveError::Unknown)?
                .kind,
        )?;
        let default_passability = PassageMatrix::<K>::get_passability(default_passage);
        let mut region_indicators =
            self.get_passable_sub_regions(passage_matrix, default_passability, coord, level)?;
        let mut regions = Vec::new();
        while !region_indicators.is_empty() {
            let mut region_index = RegionIndex::new(255);

            let mut previous_step: HashSet<RegionIndicator<CS>>;
            let mut current_step = HashSet::new();
            let mut next_step = HashSet::new();

            next_step.insert(*region_indicators.iter().last().unwrap());

            let mut count: f64 = 0.0;
            let mut cost: f64 = 0.0;

            while !next_step.is_empty() {
                previous_step = current_step;
                current_step = next_step;
                next_step = HashSet::new();
                for &RegionIndicator {
                    coordinate: sub_coord,
                    region: sub_region,
                } in &current_step
                {
                    if region_indicators.remove(&RegionIndicator {
                        coordinate: sub_coord,
                        region: sub_region,
                    }) {
                        if let Some(regions) = self
                            .passage_maps
                            .get(passage_matrix)
                            .and_then(|passage_map| passage_map.get(level + 1))
                            .and_then(|passage_level| passage_level.get(&sub_coord))
                        {
                            let region = &regions.region_indices[sub_region];
                            if PassageMatrix::<K>::get_passability(region.passage) {
                                region_index.insert(
                                    RegionIndicator {
                                        coordinate: sub_coord,
                                        region: sub_region,
                                    },
                                    &sub_coord
                                        // TODO: Make not expect
                                        .to_coord()
                                        .expect("Internal coord error")
                                        .on_edge(self.base, size)
                                        .iter()
                                        .filter(|&&direction| {
                                            region.on_edge(CS::new_direction(direction))
                                        })
                                        .cloned()
                                        .map(|direction| CS::new_direction(direction))
                                        .collect::<Vec<_>>(),
                                );
                                count += 1.0;
                                cost += (region.passage as f64
                                    + CS::DirectionType::directions()
                                        .into_iter()
                                        .map(|direction| {
                                            region.edge_passage(
                                                self.base,
                                                CS::new_direction(direction),
                                            ) as f64
                                        })
                                        .sum::<f64>())
                                    / ((CS::DirectionType::directions().len() + 1) as f64);
                            }
                        } else if default_passability {
                            region_index
                                // TODO: Make no expect
                                .insert(
                                    RegionIndicator {
                                        coordinate: sub_coord,
                                        region: sub_region,
                                    },
                                    &sub_coord
                                        .to_coord()
                                        .expect("Internal coord error")
                                        .on_edge(self.base, size)
                                        .iter()
                                        .map(|&direction| CS::new_direction(direction))
                                        .collect::<Vec<_>>(),
                                );
                            count += 1.0;
                            cost += default_passage as f64;
                        }
                        self.get_neighbor_regions(
                            &self.passage_maps[passage_matrix],
                            default_passability,
                            sub_region,
                            sub_coord,
                            level + 1,
                        )
                        .into_iter()
                        .filter(|neighbor_region| {
                            coord
                                // TODO: Make no expect
                                .to_coord()
                                .expect("Internal coord error")
                                // TODO: Make no expect
                                .next_level_coords(
                                    self.base,
                                    self.map_size.to_size().expect("Internal size error"),
                                )
                                // TODO: Make no expect
                                .contains(
                                    &neighbor_region
                                        .coordinate
                                        .to_coord()
                                        .expect("Internal coord error"),
                                )
                                && !previous_step.contains(&neighbor_region)
                                && !current_step.contains(&neighbor_region)
                                && !next_step.contains(&neighbor_region)
                        })
                        .collect::<Vec<_>>()
                        .into_iter()
                        .for_each(|neighbor| {
                            next_step.insert(neighbor);
                        });
                    }
                }
            }
            region_index.passage = match cost / count {
                p if p < 0.0 => 0,
                p if p > 255.0 => 255,
                p => p as u8,
            };
            regions.push(region_index);
        }
        Ok(regions)
    }

    pub fn get_passable_sub_regions(
        &self,
        passage_matrix: &PassageMatrix<K>,
        default_passability: bool,
        coord: CS,
        level: usize,
    ) -> Result<HashSet<RegionIndicator<CS>>, RecursiveError> {
        let mut ret = HashSet::new();
        // TODO: Make no expect
        for sub_coord in coord
            .to_coord()
            .expect("Internal coord error")
            .next_level_coords(
                self.base,
                self.level_sizes[level + 1]
                    .to_size()
                    .expect("Internal size error"),
            )
        {
            if let Some(regions) = self.passage_maps[passage_matrix]
                .get(level + 1)
                .and_then(|passage_level| passage_level.get(&CS::new_coord(sub_coord)))
            {
                for (region_number, region) in regions.region_indices.iter().enumerate() {
                    if PassageMatrix::<K>::get_passability(region.passage) {
                        ret.insert(RegionIndicator {
                            coordinate: CS::new_coord(sub_coord),
                            region: region_number,
                        });
                    }
                }
            } else if default_passability {
                ret.insert(RegionIndicator {
                    coordinate: CS::new_coord(sub_coord),
                    region: 0,
                });
            }
        }
        Ok(ret)
    }

    pub fn get_neighbor_regions(
        &self,
        passage_map: &[HashMap<CS, RecursivePassageMap<CS>>],
        default_passability: bool,
        region_number: usize,
        coord: CS,
        level: usize,
    ) -> Vec<RegionIndicator<CS>> {
        let mut ret = Vec::new();
        // TODO: Make no expect
        let neighbor_coords = coord
            .to_coord()
            .expect("Internal coord error")
            .neighbor_coords(
                self.level_sizes[level]
                    .to_size()
                    .expect("Internal size error"),
            );
        if let Some(region) = passage_map
            .get(level)
            .and_then(|passage_level| passage_level.get(&coord))
            .and_then(|regions| regions.region_indices.get(region_number))
        {
            for neighbor_coord in neighbor_coords {
                // TODO: Make no expect
                let neighbor_directions = coord
                    .to_coord()
                    .expect("Internal coord error")
                    .directions(neighbor_coord);
                if let Some(neighbor_regions) = passage_map
                    .get(level)
                    .and_then(|passage_level| passage_level.get(&CS::new_coord(neighbor_coord)))
                {
                    // Check if neighbor regions neighbor coord
                    // TODO: This causes a loop because it doesn't check for passability.  Can't require passability because then we loose the edge of each region.
                    for (i, neighbor_region) in neighbor_regions.region_indices.iter().enumerate() {
                        if !region
                            .neighbored_by(
                                neighbor_region,
                                CS::new_direction(neighbor_directions[0]),
                            )
                            .is_empty()
                        {
                            ret.push(RegionIndicator {
                                coordinate: CS::new_coord(neighbor_coord),
                                region: i,
                            });
                        }
                    }
                } else {
                    // Check if default neighbor region neighbors coord
                    if default_passability
                        && region.on_edge(CS::new_direction(neighbor_directions[0]))
                    {
                        ret.push(RegionIndicator {
                            coordinate: CS::new_coord(neighbor_coord),
                            region: 0,
                        });
                    }
                }
            }
        } else if default_passability {
            for neighbor_coord in neighbor_coords {
                if let Some(neighbor_regions) = passage_map
                    .get(level)
                    .and_then(|passage_level| passage_level.get(&CS::new_coord(neighbor_coord)))
                {
                    // Check if neighbor regions neighbor default coord
                    for (i, neighbor_region) in neighbor_regions.region_indices.iter().enumerate() {
                        if PassageMatrix::<K>::get_passability(neighbor_region.passage)
                            //                            .iter().map(|&coord| CS::new_coord(coord)).collect::<Vec<_>>()
                            // TODO: Make not expect
                            && neighbor_region.on_edge(CS::new_direction(neighbor_coord.directions(coord.to_coord().expect("Internal coord error"))[0]))
                        {
                            ret.push(RegionIndicator {
                                coordinate: CS::new_coord(neighbor_coord),
                                region: i,
                            });
                        }
                    }
                } else {
                    // Check if default neighbor region neighbors default coord (it does because of else if default_passability)
                    ret.push(RegionIndicator {
                        coordinate: CS::new_coord(neighbor_coord),
                        region: 0,
                    });
                }
            }
        }
        ret
    }

    pub fn get_sub_regions(
        &self,
        passage_map: &[HashMap<CS, RecursivePassageMap<CS>>],
        region_number: usize,
        coord: CS,
        level: usize,
    ) -> Vec<RegionIndicator<CS>> {
        let region = &passage_map[level][&coord].region_indices[region_number];

        region.region_indicators.iter().cloned().collect()
    }
}
