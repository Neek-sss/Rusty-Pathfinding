use crate::{
    data_types::{CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveKind},
    recursive_map::{RecursiveMap, RecursiveSubMap},
};
use crate::recursive_map::region_index::RegionIndicator;

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn get(&self, coord: &CS) -> &RecursiveSubMap<K> {
        // TODO: Make not expect
        let sub_coord_stack = coord
            .to_coord()
            .expect("Internal coord error")
            .as_coord_stack(self.base, self.recursive_size);
        let mut sub_map = &self.sub_maps[0][&CS::default_coord()];
        for (sub_map_level, coord) in self.sub_maps.iter().zip(sub_coord_stack.iter()) {
            if let Some(current_sub_map) = sub_map_level.get(&CS::new_coord(*coord)) {
                sub_map = current_sub_map;
            }
        }
        &sub_map
    }

    pub fn get_passability(
        &self,
        passage_matrix: &PassageMatrix<K>,
        coord: &CS,
        level_option: Option<usize>,
    ) -> Option<bool> {
        let passage_map = &self.passage_maps[passage_matrix];
        let sub_region_stack = self.get_region_stack(
            passage_map,
            // TODO: Make not expect
            &coord
                .to_coord()
                .expect("Internal coord error")
                .as_coord_stack(self.base, self.recursive_size)
                .iter()
                .map(|&coord| CS::new_coord(coord))
                .collect::<Vec<_>>(),
        );
        let level = if let Some(level_input) = level_option {
            if level_input < sub_region_stack.len() - 1 {
                Some(level_input)
            } else {
                None
            }
        } else {
            Some(sub_region_stack.len() - 1)
        };
        if let Some(mut level) = level {
            let mut passage = None;
            let mut flag = true;
            while flag {
                let RegionIndicator {
                    coordinate: coord,
                    region,
                } = sub_region_stack[level];
                if let Some(region_index) = passage_map[level][&coord].region_indices.get(region) {
                    passage = Some(region_index.passage);
                    flag = false;
                } else {
                    if level == 0 {
                        flag = false;
                    } else {
                        level -= 1;
                    }
                }
            }
            passage.and_then(|pass| Some(pass < 128))
        } else {
            None
        }
    }
}
