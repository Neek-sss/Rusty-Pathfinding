use std::collections::{HashMap, HashSet};

use crate::data_types::{CoordinateSystemTrait, RecursiveCoordinate, RecursiveDirection};

#[derive(Debug, Default, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub struct RegionIndicator<CS>
where
    CS: CoordinateSystemTrait,
{
    pub coordinate: CS,
    pub region: usize,
}

#[derive(Debug, Default, Clone)]
pub struct RegionIndex<CS>
where
    CS: CoordinateSystemTrait,
{
    pub passage: u8,
    pub region_indicators: HashSet<RegionIndicator<CS>>,
    pub region_edges: HashMap<CS, Vec<RegionIndicator<CS>>>,
}

impl<CS> RegionIndex<CS>
where
    CS: CoordinateSystemTrait,
{
    pub fn new(passage: u8) -> Self {
        let mut region_edges = HashMap::new();
        for direction in CS::DirectionType::directions() {
            region_edges.insert(CS::new_direction(direction), Vec::new());
        }
        Self {
            passage,
            region_indicators: HashSet::new(),
            region_edges,
        }
    }

    pub fn insert(&mut self, region_indicator: RegionIndicator<CS>, edge_directions: &[CS]) {
        self.region_indicators.insert(region_indicator);
        for direction in edge_directions {
            self.region_edges
                .get_mut(direction)
                .unwrap()
                .push(region_indicator);
        }
    }

    pub fn on_edge(&self, direction: CS) -> bool {
        !self.region_edges[&direction].is_empty()
    }

    pub fn neighbored_by(&self, neighbor: &Self, direction: CS) -> Vec<RegionIndicator<CS>> {
        self.region_edges[&direction]
            .iter()
            .filter(|sub_region_indicator| {
                // No entry found for key
                // TODO: Make not expect
                neighbor.region_edges[&CS::new_direction(
                    direction
                        .to_direction()
                        .expect("Internal direction error")
                        .opposite(),
                )]
                    .iter()
                    .any(|neighbor_region_indicator| {
                        sub_region_indicator
                            .coordinate
                            // TODO: Make not expect
                            .to_coord()
                            .expect("Internal coord error")
                            // TODO: Make not expect
                            .neighbors(
                                neighbor_region_indicator
                                    .coordinate
                                    .to_coord()
                                    .expect("Internal coord error"),
                            )
                    })
            })
            .cloned()
            .collect()
    }

    pub fn edge_passage(&self, base: usize, direction: CS) -> u8 {
        let passage = 255.0 * (1.0 - (self.region_edges[&direction].len() as f64 / base as f64));
        match passage {
            p if p < 0.0 => 0,
            p if p > 255.0 => 255,
            p => p as u8,
        }
    }
}
