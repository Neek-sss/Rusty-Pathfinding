#[cfg(feature = "amethyst_system")]
use amethyst::ecs::{Component, VecStorage};

use crate::data_types::{CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveKind};
use crate::recursive_map::region_index::RegionIndicator;

#[derive(Debug, Clone, Default)]
pub struct RecursivePath<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub passage_matrix: PassageMatrix<K>,
    pub from: CS,
    pub to: CS,
    pub base: usize,
    pub recursive_size: usize,
    pub level_sizes: Vec<CS>,
    pub map_size: CS,
    pub recursive_slice: Vec<Vec<(usize, RegionIndicator<CS>)>>,
}

impl<CS, K> RecursivePath<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn pop(&mut self) -> Option<CS> {
        if self.from == self.to {
            if self.peek().is_some() {
                self.recursive_slice = Vec::new();
            }
            None
        } else {
            let ret = self.peek();
            if let Some(ret_inner) = ret {
                if ret_inner == self.to {
                    self.recursive_slice = Vec::new();
                } else {
                    self.remove_coord(ret_inner);
                    self.from = ret_inner;
                }
            }
            ret
        }
    }

    pub fn peek(&self) -> Option<CS> {
        self.get_next_coord(self.from)
    }

    pub fn get_next_coord(&self, from_coord: CS) -> Option<CS> {
        if self.recursive_slice.is_empty() {
            None
        } else {
            let direction = self.get_next_direction(from_coord);
            println!("dir: {direction:?}");

            self.from.to_coord().ok().and_then(|coord| {
                direction.to_direction().ok().and_then(|dir| {
                    Some(CoordinateSystemTrait::new_coord(coord.add_direction(dir)))
                })
            })
        }
    }

    pub fn get_next_direction(&self, from_coord: CS) -> CS {
        let from_coord_stack = from_coord
            .to_coord()
            .unwrap()
            .as_coord_stack(self.base, self.recursive_size);

        println!("fcs: {from_coord_stack:?}");

        let mut last_match_level = self
            .recursive_slice
            .iter()
            .zip(from_coord_stack.iter().cloned())
            .filter(|(level, from_coord)| {
                level[0].1.coordinate == CS::new_coord(from_coord.clone())
            })
            .count()
            - 1;

        println!("lml (before): {last_match_level}");

        while self.recursive_slice[last_match_level].len() == 1 && last_match_level != 0 {
            last_match_level = last_match_level.checked_sub(1).unwrap_or(0);
        }
        println!("p: {:?}", self.recursive_slice);
        println!("lml: {last_match_level}");

        if self.recursive_slice.len() == 1 {
            println!("Straight line to end");
            CS::new_direction(
                self.from
                    .to_coord()
                    .unwrap()
                    .directions(self.to.to_coord().unwrap())[0],
            )
        } else {
            let next_match_level = self
                .recursive_slice
                .windows(2)
                .enumerate()
                .filter(|&(i, levels)| {
                    let parent_index = if i < last_match_level { 1 } else { 0 };
                    let parent = levels[1].get(0);

                    let child_index = if last_match_level < i + 1 { 1 } else { 0 };
                    let child = levels[0].get(0);

                    if let (Some(parent), Some(child)) = (parent, child) {
                        parent
                            .1
                            .coordinate
                            .to_coord()
                            .unwrap()
                            .convert_upwards(1, self.base)
                            == child.1.coordinate.to_coord().unwrap()
                    } else {
                        true
                    }
                })
                .count();
            println!("nml: {next_match_level}");

            if next_match_level == from_coord_stack.len() - 1 {
                let directions = from_coord.to_coord().unwrap().directions(
                    self.recursive_slice[next_match_level][0]
                        .1
                        .coordinate
                        .to_coord()
                        .unwrap(),
                );

                println!("dirs: {directions:?}");

                let coords = directions
                    .iter()
                    .cloned()
                    .map(|direction| from_coord.to_coord().unwrap().add_direction(direction))
                    .collect::<Vec<_>>();

                if let Some(i) = coords.iter().cloned().position(|coord| {
                    self.recursive_slice
                        .get(from_coord_stack.len() - 1)
                        .and_then(|s| s.get(0))
                        .filter(|r| r.1.coordinate.to_coord().unwrap() == coord)
                        .is_some()
                }) {
                    println!("Exact next position");
                    CS::new_direction(directions[i])
                } else if let Some(i) = coords.iter().cloned().position(|coord| {
                    from_coord_stack[from_coord_stack.len() - 2]
                        == coord.convert_upwards(1, self.base)
                }) {
                    println!("Staying in parent region");
                    CS::new_direction(directions[i])
                } else {
                    println!("Error: Direction did not change");
                    CS::default_direction()
                }
            } else {
                println!("Empty next level");
                println!("from: {:?}", from_coord_stack[next_match_level]);
                println!("slice: {:?}", self.recursive_slice[next_match_level][0]);
                let directions = from_coord_stack[next_match_level].directions(
                    self.recursive_slice[next_match_level][0]
                        .1
                        .coordinate
                        .to_coord()
                        .unwrap(),
                );
                println!("dirs: {directions:?}");
                CS::new_direction(directions[0])
            }
        }
    }

    pub fn remove_coord(&mut self, coord: CS) {
        if !self.recursive_slice.is_empty() {
            let coord_stack = coord
                .to_coord()
                .unwrap()
                .as_coord_stack(self.base, self.recursive_size);

            let bottom_rec_slice_index = self.recursive_slice.len() - 1;

            if let Some(bottom_slice) = self.recursive_slice.get_mut(bottom_rec_slice_index) {
                if bottom_slice[0].1.coordinate.to_coord().unwrap()
                    == coord_stack[bottom_rec_slice_index]
                {
                    bottom_slice.remove(0);
                }
            }

            for i in (0..self.recursive_slice.len() - 1).rev() {
                if (self.recursive_slice[i + 1].is_empty()
                    || self.recursive_slice[i][0].1.coordinate.to_coord().unwrap()
                        != self.recursive_slice[i + 1][0]
                            .1
                            .coordinate
                            .to_coord()
                            .unwrap()
                            .convert_upwards(1, self.base))
                    && self.recursive_slice[i][0].1.coordinate.to_coord().unwrap() == coord_stack[i]
                {
                    self.recursive_slice[i].remove(0);
                }
            }

            while self.recursive_slice.len() > 1
                && self.recursive_slice[self.recursive_slice.len() - 1].is_empty()
            {
                self.recursive_slice.pop();
            }
        }
    }
}

impl<CS, K> Iterator for RecursivePath<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    type Item = CS;
    fn next(&mut self) -> Option<Self::Item> {
        self.pop()
    }
}

#[cfg(feature = "amethyst_system")]
impl<CS, K> Component for RecursivePath<CS, K>
where
    CS: 'static + CoordinateSystemTrait,
    K: 'static + RecursiveKind,
{
    type Storage = VecStorage<Self>;
}
