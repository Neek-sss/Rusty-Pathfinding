use std::collections::{HashMap, HashSet};

use crate::{
    data_types::{
        CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveDirection,
        RecursiveError, RecursiveKind,
    },
    recursive_map::{RecursiveMap, RecursivePath, RegionIndex},
};
use crate::recursive_map::region_index::RegionIndicator;
use crate::recursive_map::rmap::RecursivePassageMap;

#[derive(Debug)]
enum CostMapRet<'a, CS>
where
    CS: 'a + CoordinateSystemTrait,
{
    CostMap {
        version: usize,
        region: &'a RegionIndex<CS>,
        map: HashMap<RegionIndicator<CS>, f64>,
    },
    Edge(Vec<RegionIndicator<CS>>),
    Vector {
        region: RegionIndicator<CS>,
        level: usize,
        direction: CS,
    },
}

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn new_path(
        &self,
        passage_matrix: &PassageMatrix<K>,
        from: CS,
        to: CS,
    ) -> RecursivePath<CS, K> {
        RecursivePath {
            passage_matrix: passage_matrix.clone(),
            from,
            to,
            base: self.base,
            recursive_size: self.recursive_size,
            map_size: self.map_size,
            level_sizes: self.level_sizes.clone(),
            ..RecursivePath::default()
        }
    }

    pub fn refine_path(
        &mut self,
        recursive_path: &mut RecursivePath<CS, K>,
    ) -> Result<bool, RecursiveError> {
        let from_passage = recursive_path
            .passage_matrix
            .get_passage(self.get(&recursive_path.from).kind)
            .expect("Passage error");
        let from_passable = PassageMatrix::<K>::get_passability(from_passage);

        let to_passage = recursive_path
            .passage_matrix
            .get_passage(self.get(&recursive_path.to).kind)
            .expect("Passage error");
        let to_passable = PassageMatrix::<K>::get_passability(to_passage);

        let passage_map = &self.passage_maps[&recursive_path.passage_matrix];

        let from_coord_stack = recursive_path
            .from
            // TODO: Make not expect
            .to_coord()
            .expect("Internal coord error")
            .as_coord_stack(self.base, self.recursive_size)
            .iter()
            .map(|&coord| CS::new_coord(coord))
            .collect::<Vec<_>>();
        let from_region_stack = self.get_region_stack(passage_map, &from_coord_stack);

        let to_coord_stack = recursive_path
            .to
            // TODO: Make not expect
            .to_coord()
            .expect("Internal coord error")
            .as_coord_stack(self.base, self.recursive_size)
            .iter()
            .map(|&coord| CS::new_coord(coord))
            .collect::<Vec<_>>();
        let to_region_stack = self.get_region_stack(passage_map, &to_coord_stack);

        let is_outdated_version = recursive_path
            .recursive_slice
            .iter()
            .zip(passage_map.iter())
            .any(|(path_level, passage_level)| {
                path_level.iter().any(
                    |&(
                        version,
                        RegionIndicator {
                            coordinate: coord, ..
                        },
                    )| {
                        passage_level
                            .get(&coord)
                            .and_then(|passage| Some(version != passage.version))
                            .unwrap_or(false)
                    },
                )
            });

        if !from_passable || !to_passable {
            Ok(false)
        } else if is_outdated_version {
            *recursive_path = self.new_path(
                &recursive_path.passage_matrix,
                recursive_path.from,
                recursive_path.to,
            );
            Ok(true)
        } else if from_coord_stack.len() == recursive_path.recursive_slice.len() {
            Ok(false)
        } else {
            if recursive_path.recursive_slice.is_empty() {
                let max_level = from_region_stack.len().max(to_region_stack.len());
                let start_level = from_region_stack
                    .iter()
                    .chain(
                        vec![
                            RegionIndicator {
                                coordinate: CS::default_coord(),
                                region: usize::MAX,
                            };
                            max_level - from_region_stack.len()
                        ]
                        .iter(),
                    )
                    .zip(
                        to_region_stack.iter().chain(
                            vec![
                                RegionIndicator {
                                    coordinate: CS::default_coord(),
                                    region: usize::MAX,
                                };
                                max_level - to_region_stack.len()
                            ]
                            .iter(),
                        ),
                    )
                    .position(|(from, to)| *from != *to)
                    .and_then(|position| position.checked_sub(1))
                    .unwrap_or(0);

                for i in 0..=start_level {
                    recursive_path.recursive_slice.push(vec![(
                        self.passage_maps[&recursive_path.passage_matrix][i]
                            [&from_region_stack[i].coordinate]
                            .version,
                        from_region_stack[i],
                    )]);
                }
            }

            let current_level = recursive_path
                .recursive_slice
                .iter()
                .filter(|level| !level.is_empty())
                .count()
                - 1;

            self.full_path(
                passage_map,
                current_level,
                &from_coord_stack,
                &to_coord_stack,
                &from_region_stack,
                &to_region_stack,
                recursive_path,
            )?;

            if let Some(path_val) = recursive_path
                .recursive_slice
                .get(from_coord_stack.len() - 1)
                .cloned()
            {
                let bottom_rec_slice_index = recursive_path.recursive_slice.len() - 1;

                if let Some(bottom_slice) = recursive_path
                    .recursive_slice
                    .get_mut(bottom_rec_slice_index)
                {
                    if bottom_slice[0].1.coordinate == from_coord_stack[bottom_rec_slice_index] {
                        bottom_slice.remove(0);
                    }
                }

                for i in (0..recursive_path.recursive_slice.len() - 1).rev() {
                    if (recursive_path.recursive_slice[i + 1].is_empty()
                        || recursive_path.recursive_slice[i][0]
                            .1
                            .coordinate
                            .to_coord()
                            .unwrap()
                            != recursive_path.recursive_slice[i + 1][0]
                                .1
                                .coordinate
                                .to_coord()
                                .unwrap()
                                .convert_upwards(1, self.base))
                        && recursive_path.recursive_slice[i][0].1.coordinate == from_coord_stack[i]
                    {
                        recursive_path.recursive_slice[i].remove(0);
                    }
                }

                while recursive_path.recursive_slice.len() > 0
                    && recursive_path.recursive_slice[recursive_path.recursive_slice.len() - 1]
                        .is_empty()
                {
                    recursive_path.recursive_slice.pop();
                }
            }

            let new_level = recursive_path
                .recursive_slice
                .iter()
                .filter(|level| !level.is_empty())
                .count()
                .checked_sub(1)
                .unwrap_or(0);

            if new_level == current_level {
                Ok(false)
            } else {
                Ok(true)
            }
        }
    }

    pub fn full_path(
        &self,
        passage_map: &[HashMap<CS, RecursivePassageMap<CS>>],
        parent_level: usize,
        from_coord_stack: &[CS],
        to_coord_stack: &[CS],
        from_region_stack: &[RegionIndicator<CS>],
        to_region_stack: &[RegionIndicator<CS>],
        recursive_path: &mut RecursivePath<CS, K>,
    ) -> Result<(), RecursiveError> {
        println!("pl: {parent_level}");
        println!();
        let child_level = parent_level + 1;

        let mut last_path_val: Option<RegionIndicator<CS>> = None;

        let mut parent_path = recursive_path.recursive_slice[parent_level]
            .iter()
            .map(|r| Some(r.clone()))
            .collect::<Vec<_>>();
        parent_path.insert(0, None);
        parent_path.push(None);

        let mut i = 0;
        // TODO: Can be parallelized
        for parent_path_section in parent_path.windows(3) {
            println!("window {i}");
            i += 1;
            let (parent_version, parent_region) = parent_path_section[1].unwrap();

            let last_parent_region_opt = parent_path_section[0].and_then(|r| Some(r.1));
            let next_parent_region_opt = parent_path_section[2].and_then(|r| Some(r.1));

            println!("pr: {parent_region:?}");

            let has_sub_regions = passage_map
                .get(parent_level)
                .and_then(|l| l.get(&parent_region.coordinate))
                .and_then(|m| m.region_indices.get(parent_region.region))
                .and_then(|r| Some(!r.region_indicators.is_empty()))
                .unwrap_or(false);

            if has_sub_regions {
                let parent_region_index = &passage_map[parent_level][&parent_region.coordinate]
                    .region_indices[parent_region.region];

                let parent_passability =
                    PassageMatrix::<K>::get_passability(parent_region_index.passage);

                let to_vec = if let Some(next_parent_region) = next_parent_region_opt {
                    let directions = parent_region
                        .coordinate
                        .to_coord()
                        .expect("Not a coordinate")
                        .directions(
                            next_parent_region
                                .coordinate
                                .to_coord()
                                .expect("Not a coordinate"),
                        );

                    if self
                        .get_neighbor_regions(
                            passage_map,
                            parent_passability,
                            parent_region.region,
                            parent_region.coordinate,
                            parent_level,
                        )
                        .contains(&next_parent_region)
                    {
                        let mut ret = Vec::new();
                        for region in &passage_map[parent_level][&parent_region.coordinate]
                            .region_indices[parent_region.region]
                            .region_edges[&CS::new_direction(directions[0])]
                        {
                            if !passage_map[parent_level]
                                .contains_key(&next_parent_region.coordinate)
                                || passage_map[parent_level][&next_parent_region.coordinate]
                                    .region_indices[next_parent_region.region]
                                    .region_edges[&CS::new_direction(directions[0].opposite())]
                                    .iter()
                                    .any(|neighbor_region| {
                                        self.get_neighbor_regions(
                                            passage_map,
                                            parent_passability,
                                            neighbor_region.region,
                                            neighbor_region.coordinate,
                                            child_level,
                                        )
                                        .contains(region)
                                    })
                            {
                                ret.push(region.clone());
                            }
                        }
                        ret
                    } else {
                        passage_map[parent_level][&parent_region.coordinate].region_indices
                            [parent_region.region]
                            .region_edges[&CS::new_direction(directions[0])]
                            .clone()
                    }
                } else {
                    if let Some(to_child_region) = to_region_stack.get(child_level) {
                        vec![to_child_region.clone()]
                    } else {
                        // TODO: figure out default to_child_region
                        vec![RegionIndicator {
                            coordinate: to_coord_stack[child_level],
                            region: 0,
                        }]
                    }
                };

                // TODO: Use a heap instead of a vec
                let mut regions_to_search = to_vec
                    .iter()
                    .map(|region| (region.clone(), 0))
                    .collect::<Vec<_>>();

                let mut searched_regions = to_vec.iter().cloned().collect::<HashSet<_>>();
                let mut cost_map = regions_to_search.iter().cloned().collect::<HashMap<_, _>>();
                let allowed_regions = self.get_sub_regions(
                    passage_map,
                    parent_region.region,
                    parent_region.coordinate,
                    parent_level,
                );

                while !regions_to_search.is_empty() {
                    regions_to_search.sort_by(|(_, cost1), (_, cost2)| cost2.cmp(cost1));
                    let (current_region, current_cost) = regions_to_search.pop().unwrap();

                    for neighbor in self.get_neighbor_regions(
                        passage_map,
                        parent_passability,
                        current_region.region,
                        current_region.coordinate,
                        child_level,
                    ) {
                        if !searched_regions.contains(&neighbor)
                            && allowed_regions.contains(&neighbor)
                        {
                            searched_regions.insert(neighbor);
                            regions_to_search.push((neighbor, current_cost + 1));
                            cost_map.insert(neighbor, current_cost + 1);
                        }
                    }
                }

                let from_region = if let Some(last_path_region) = last_path_val {
                    let mut ret = RegionIndicator::<CS>::default();
                    for region in self.get_neighbor_regions(
                        passage_map,
                        parent_passability,
                        last_path_region.region,
                        last_path_region.coordinate,
                        child_level,
                    ) {
                        if passage_map[parent_level][&parent_region.coordinate].region_indices
                            [parent_region.region]
                            .region_indicators
                            .contains(&region)
                        {
                            ret = region;
                        }
                    }
                    ret
                } else {
                    if let Some(from_child_region) = from_region_stack.get(child_level) {
                        from_child_region.clone()
                    } else {
                        RegionIndicator {
                            coordinate: from_coord_stack[child_level],
                            region: 0,
                        }
                    }
                };

                let mut region = from_region;
                let mut cost = cost_map[&region];

                loop {
                    let mut next_cost = None;
                    let mut next_region = None;

                    for neighbor in self.get_neighbor_regions(
                        passage_map,
                        true,
                        region.region,
                        region.coordinate,
                        child_level,
                    ) {
                        if let Some(&neighbor_cost) = cost_map.get(&neighbor) {
                            if neighbor_cost < cost {
                                next_cost = Some(neighbor_cost);
                                next_region = Some(neighbor);
                            }
                        }
                    }

                    if recursive_path.recursive_slice.len() <= child_level {
                        recursive_path.recursive_slice.push(Vec::new());
                    }
                    if let Some(passage) = passage_map
                        .get(child_level)
                        .and_then(|c| c.get(&region.coordinate))
                    {
                        recursive_path.recursive_slice[child_level].push((passage.version, region));
                        last_path_val = Some(region);
                    } else {
                        recursive_path.recursive_slice[child_level].push((parent_version, region));
                        last_path_val = Some(region);
                    }

                    if let (Some(next_cost), Some(next_region)) = (next_cost, next_region) {
                        region = next_region;
                        cost = next_cost;
                    } else {
                        break;
                    }
                }
            } else {
                if let Some(last_path_region) = last_path_val {
                    if let Some(next_parent_region) = next_parent_region_opt {
                        let parent_coord = parent_region.coordinate.to_coord().unwrap();

                        let directions = parent_coord.directions(
                            next_parent_region
                                .coordinate
                                .to_coord()
                                .expect("Not a coord"),
                        );

                        let sub_coords = parent_coord.next_level_coords(
                            self.base,
                            self.level_sizes[child_level].to_size().expect("Not a size"),
                        );

                        let next_region_index_opt = &passage_map[parent_level]
                            .get(&next_parent_region.coordinate)
                            .and_then(|c| c.region_indices.get(next_parent_region.region));

                        if let Some(next_region_index) = next_region_index_opt {
                            let edge = next_region_index
                                .region_edges
                                .get(&CS::new_direction(directions[0].opposite()))
                                .unwrap();

                            let mut distance = usize::MAX;

                            for coord in sub_coords {
                                let neighbor_coord = coord.add_direction(directions[0]);

                                let on_edge_in_neighbor = edge.iter().any(|ind| {
                                    ind.coordinate.to_coord().expect("Not a coord")
                                        == neighbor_coord
                                });

                                let new_dist = coord.distance(
                                    last_path_region.coordinate.to_coord().expect("Not a coord"),
                                );

                                if on_edge_in_neighbor && new_dist < distance {
                                    distance = new_dist;
                                    last_path_val = Some(RegionIndicator {
                                        coordinate: CS::new_coord(coord),
                                        region: 0,
                                    });
                                }
                            }
                        } else {
                            let neighbor_sub_coords = next_parent_region
                                .coordinate
                                .to_coord()
                                .expect("Not a coord")
                                .next_level_coords(
                                    self.base,
                                    self.level_sizes[child_level].to_size().expect("Not a size"),
                                );

                            let mut distance = usize::MAX;

                            for coord in sub_coords {
                                let neighbor_coord = coord.add_direction(directions[0]);

                                let on_edge_in_direction =
                                    neighbor_sub_coords.contains(&neighbor_coord);

                                let new_dist = coord.distance(
                                    last_path_region.coordinate.to_coord().expect("Not a coord"),
                                );

                                if on_edge_in_direction && new_dist < distance {
                                    distance = new_dist;
                                    last_path_val = Some(RegionIndicator {
                                        coordinate: CS::new_coord(coord),
                                        region: 0,
                                    });
                                }
                            }
                        }
                    } else {
                        last_path_val = None;
                    }
                } else {
                    if let Some(from_child_region) = from_region_stack.get(child_level) {
                        last_path_val = Some(from_child_region.clone());
                    } else {
                        last_path_val = Some(RegionIndicator {
                            coordinate: from_coord_stack[child_level],
                            region: 0,
                        });
                    }
                }
            }
        }

        Ok(())
    }

    pub fn get_region_stack(
        &self,
        passage_map: &[HashMap<CS, RecursivePassageMap<CS>>],
        coord_stack: &[CS],
    ) -> Vec<RegionIndicator<CS>> {
        let mut region_stack = Vec::new();
        for (&coord, passage_level) in coord_stack.iter().zip(passage_map.iter()).rev() {
            if let Some(passage_point) = passage_level.get(&coord) {
                if let Some(&sub_region) = region_stack.iter().last() {
                    if let Some(region_index) = passage_point
                        .region_indices
                        .iter()
                        .position(|region| region.region_indicators.contains(&sub_region))
                    {
                        region_stack.push(RegionIndicator {
                            coordinate: coord,
                            region: region_index,
                        });
                    } else {
                        // TODO: Make this a consistent change across the board
                        region_stack.push(RegionIndicator {
                            coordinate: coord,
                            region: usize::max_value(),
                        });
                    }
                } else {
                    region_stack.push(RegionIndicator {
                        coordinate: coord,
                        region: 0,
                    });
                }
            }
        }
        region_stack.into_iter().rev().collect()
    }
}
