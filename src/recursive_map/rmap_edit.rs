use crate::{
    data_types::{CoordinateSystemTrait, RecursiveCoordinate, RecursiveKind},
    recursive_map::{RecursiveMap, RecursiveSubMap},
};

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn edit(&mut self, coord: CS, new_kind: K) {
        // TODO: Make not expect
        let sub_coord_stack = coord
            .to_coord()
            .expect("Internal coord error")
            .as_coord_stack(self.base, self.recursive_size);
        let mut current_kind = self.sub_maps[0][&CS::default_coord()].kind;
        for (sub_map_level, coord) in self.sub_maps.iter().zip(sub_coord_stack.iter()) {
            if let Some(sub_map) = sub_map_level.get(&CS::new_coord(*coord)) {
                current_kind = sub_map.kind;
            } else {
                break;
            }
        }

        if current_kind != new_kind {
            for (sub_map_level, coord) in self.sub_maps.iter_mut().zip(sub_coord_stack.iter()) {
                sub_map_level
                    .entry(CS::new_coord(*coord))
                    .or_insert(RecursiveSubMap {
                        version: 1,
                        edit_version: 1,
                        kind: current_kind,
                    });
            }
            self.sub_maps[self.recursive_size - 1]
                .get_mut(&CS::new_coord(sub_coord_stack[self.recursive_size - 1]))
                .unwrap()
                .kind = new_kind;
            for (sub_map_level, coord) in self.sub_maps.iter_mut().zip(sub_coord_stack.iter()) {
                let sub_map = sub_map_level.get_mut(&CS::new_coord(*coord)).unwrap();
                sub_map.edit_version = sub_map.edit_version.wrapping_add(1);
            }
        }
    }
}
