use crate::{
    data_types::{CoordinateSystemTrait, RecursiveCoordinate, RecursiveKind},
    recursive_map::{RecursiveMap, RecursiveSubMap},
};

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn stack_func(
        &self,
        down_filter: &dyn Fn(usize, CS, Option<&RecursiveSubMap<K>>) -> bool,
        return_filter: Option<&dyn Fn(usize, CS, Option<&RecursiveSubMap<K>>) -> bool>,
    ) -> Vec<Vec<CS>> {
        let mut ret = vec![Vec::new(); self.recursive_size];
        let mut next_level_option = Some(vec![CS::default_coord()]);
        for level in 0..self.recursive_size {
            if let Some(this_level) = next_level_option.take() {
                let mut next_level_stack = Vec::new();
                for coord in this_level {
                    let sub_map_option = self.sub_maps[level].get(&coord);
                    if down_filter(level, coord, sub_map_option) {
                        // TODO: Make this not expect
                        for next_coord in coord
                            .to_coord()
                            .expect("Internal coord error")
                            .next_level_coords(
                                self.base,
                                self.map_size.to_size().expect("Internal size error"),
                            )
                        {
                            next_level_stack.push(CS::new_coord(next_coord));
                        }
                    }
                    if let Some(return_filter_inner) = return_filter {
                        if return_filter_inner(level, coord, sub_map_option) {
                            ret[level].push(coord);
                        }
                    } else if down_filter(level, coord, sub_map_option) {
                        ret[level].push(coord);
                    }
                }
                if !next_level_stack.is_empty() {
                    next_level_option = Some(next_level_stack);
                }
            }
        }
        ret
    }

    pub fn up_stack(
        &self,
        down_filter: &dyn Fn(usize, CS, Option<&RecursiveSubMap<K>>) -> bool,
    ) -> Vec<Vec<CS>> {
        self.stack_func(down_filter, None)
    }

    pub fn down_stack(
        &self,
        down_filter: &dyn Fn(usize, CS, Option<&RecursiveSubMap<K>>) -> bool,
        return_filter: &dyn Fn(usize, CS, Option<&RecursiveSubMap<K>>) -> bool,
    ) -> Vec<Vec<CS>> {
        self.stack_func(down_filter, Some(return_filter))
    }
}
