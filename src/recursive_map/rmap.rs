use std::collections::HashMap;

use serde_derive::{Deserialize, Serialize};

use crate::{
    data_types::{
        CoordinateSystemTrait, PassageMatrix, RecursiveCoordinate, RecursiveKind, RecursiveSize,
    },
    recursive_map::RegionIndex,
};
use crate::data_types::CoordinateSystemUSize3;

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct RecursiveSubMap<K>
where
    K: RecursiveKind,
{
    pub version: usize,
    pub edit_version: usize,
    pub kind: K,
}

#[derive(Debug, Clone, Default)]
pub struct RecursivePassageMap<CS>
where
    CS: CoordinateSystemTrait,
{
    pub version: usize,
    pub region_indices: Vec<RegionIndex<CS>>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub map_size: CS,
    pub base: usize,
    pub recursive_size: usize,
    pub level_sizes: Vec<CS>,
    pub sub_maps: Vec<HashMap<CS, RecursiveSubMap<K>>>,
    #[serde(skip)]
    pub passage_maps: HashMap<PassageMatrix<K>, Vec<HashMap<CS, RecursivePassageMap<CS>>>>,
}

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn new(size: CS, base: usize, default_kind: K) -> Self {
        // TODO: Make not expect
        let level_sizes = size
            .to_size()
            .expect("Internal size error")
            .as_size_stack(base)
            .iter()
            .map(|&size| CS::new_size(size))
            .collect::<Vec<_>>();
        let recursive_size = level_sizes.len();
        let mut sub_maps = Vec::new();
        for _ in 0..recursive_size {
            sub_maps.push(HashMap::new());
        }
        sub_maps[0].insert(
            CS::default_coord(),
            RecursiveSubMap {
                version: 1,
                edit_version: 1,
                kind: default_kind,
            },
        );
        Self {
            map_size: size,
            base,
            recursive_size,
            level_sizes,
            sub_maps,
            ..Self::default()
        }
    }

    pub fn empty_sub_map(&self, level: usize, coord: CS) -> bool {
        level + 1 >= self.recursive_size
            || !coord
            // TODO: Make not expect
            .to_coord()
            .expect("Internal coord error")
            .next_level_coords(
                self.base,
                self.map_size.to_size().expect("Internal size error"),
            )
            .iter()
            .any(|sub_coord| {
                self.sub_maps[level + 1].contains_key(&CS::new_coord(*sub_coord))
            })
    }
}

impl<K> RecursiveMap<CoordinateSystemUSize3, K>
where
    K: RecursiveKind + std::fmt::Display,
{
    pub fn as_path_string(
        &self,
        from: &CoordinateSystemUSize3,
        path: &[CoordinateSystemUSize3],
    ) -> String {
        let mut ret = String::new();

        let size = self.map_size.to_size().unwrap();
        let path_coords = path
            .iter()
            .cloned()
            .filter_map(|c| c.to_coord().ok())
            .collect::<Vec<_>>();

        let last_coord = path_coords.iter().last().cloned();
        let first_coord = from.to_coord().ok();

        for z in 0..size[2] {
            for y in 0..size[1] {
                for x in 0..size[0] {
                    if first_coord.is_some() && [x, y, z] == first_coord.unwrap() {
                        ret += "F";
                    } else if last_coord.is_some() && [x, y, z] == last_coord.unwrap() {
                        ret += "T";
                    } else if path_coords.contains(&[x, y, z]) {
                        ret += "*"
                    } else {
                        ret += &format!(
                            "{}",
                            self.get(&CoordinateSystemUSize3::new_coord([x, y, z])).kind
                        );
                    }
                    if x < size[0] - 1 {
                        ret += "\t"
                    }
                }
                ret += "\n"
            }
            if z < size[2] - 1 {
                ret += &(0..size[0]).map(|_| "=").collect::<String>();
                ret += "\n";
            }
        }

        ret
    }

    pub fn as_kind_string(&self) -> String {
        let mut ret = String::new();

        let size = self.map_size.to_size().unwrap();

        for z in 0..size[2] {
            for y in 0..size[1] {
                for x in 0..size[0] {
                    ret += &format!(
                        "{}",
                        self.get(&CoordinateSystemUSize3::new_coord([x, y, z])).kind
                    );
                    if x < size[0] - 1 {
                        ret += "\t"
                    }
                }
                ret += "\n"
            }
            if z < size[2] - 1 {
                ret += &(0..size[0]).map(|_| "=").collect::<String>();
                ret += "\n";
            }
        }

        ret
    }

    pub fn as_recursive_kind_string(&self) -> String {
        let mut ret = String::new();

        let level_sizes = self
            .level_sizes
            .iter()
            .filter_map(|c| c.to_size().ok())
            .collect::<Vec<_>>();
        let sub_maps = &self.sub_maps;

        for sub_map_index in 0..sub_maps.len() {
            for z in 0..level_sizes[sub_map_index][2] {
                for y in 0..level_sizes[sub_map_index][1] {
                    for x in 0..level_sizes[sub_map_index][0] {
                        if let Some(sub_map) = sub_maps[sub_map_index]
                            .get(&CoordinateSystemUSize3::new_coord([x, y, z]))
                        {
                            ret += &format!("{}", sub_map.kind);
                        } else {
                            ret += "_"
                        }
                        if x < level_sizes[sub_map_index][0] - 1 {
                            ret += "\t"
                        }
                    }
                    ret += "\n"
                }
                if z < level_sizes[sub_map_index][2] - 1 {
                    ret += &(0..level_sizes[sub_map_index][0])
                        .map(|_| "=")
                        .collect::<String>();
                    ret += "\n";
                }
            }
            if sub_map_index < sub_maps.len() - 1 {
                ret += "<--->\n";
            }
        }

        ret
    }

    pub fn as_passability_string(&self, passage_matrix: &PassageMatrix<K>) -> String {
        let mut ret = String::new();

        let level_sizes = self
            .level_sizes
            .iter()
            .filter_map(|c| c.to_size().ok())
            .collect::<Vec<_>>();
        let passage_maps = &self.passage_maps[passage_matrix];

        for passage_map_index in 0..passage_maps.len() {
            for z in 0..level_sizes[passage_map_index][2] {
                for y in 0..level_sizes[passage_map_index][1] {
                    for x in 0..level_sizes[passage_map_index][0] {
                        if let Some(passage_map) = passage_maps[passage_map_index]
                            .get(&CoordinateSystemUSize3::new_coord([x, y, z]))
                        {
                            for region in &passage_map.region_indices {
                                ret += if PassageMatrix::<K>::get_passability(region.passage) {
                                    "P"
                                } else {
                                    "I"
                                }
                            }
                        } else {
                            ret += "_"
                        }
                        if x < level_sizes[passage_map_index][0] - 1 {
                            ret += "\t"
                        }
                    }
                    ret += "\n"
                }
                if z < level_sizes[passage_map_index][2] - 1 {
                    ret += &(0..level_sizes[passage_map_index][0])
                        .map(|_| "=")
                        .collect::<String>();
                    ret += "\n";
                }
            }
            if passage_map_index < passage_maps.len() - 1 {
                ret += "<--->\n";
            }
        }

        ret
    }
}
