use std::collections::HashMap;

use crate::{
    data_types::{CoordinateSystemTrait, RecursiveCoordinate, RecursiveError, RecursiveKind},
    recursive_map::{RecursiveMap, RecursiveSubMap},
};

impl<CS, K> RecursiveMap<CS, K>
where
    CS: CoordinateSystemTrait,
    K: RecursiveKind,
{
    pub fn get_max_kind(&self, level: usize, coord: CS, current_kind: K) -> K {
        let mut counts = HashMap::new();
        // TODO: Make no expect
        for next_coord in coord
            .to_coord()
            .expect("Internal coord error")
            .next_level_coords(
                self.base,
                self.map_size.to_size().expect("Internal size error"),
            )
        {
            if !self.sub_maps[level + 1].contains_key(&CS::new_coord(next_coord)) {
                *counts.entry(current_kind).or_insert(0) += 1;
            } else if self.empty_sub_map(level, CS::new_coord(next_coord)) {
                *counts
                    .entry(self.sub_maps[level + 1][&CS::new_coord(next_coord)].kind)
                    .or_insert(0) += 1;
            }
        }

        counts
            .iter()
            .fold((current_kind, 0), |acc, count| {
                if *count.1 < acc.1 && *count.0 < acc.0 {
                    (*count.0, *count.1)
                } else {
                    acc
                }
            })
            .0
    }

    pub fn compress(&mut self) -> Result<(), RecursiveError> {
        let up_stack = self.up_stack(&|_, _, sub_map_option| {
            if let Some(sub_map) = sub_map_option {
                sub_map.version != sub_map.edit_version
            } else {
                false
            }
        });

        for (level, coords) in up_stack.iter().enumerate().rev() {
            if level < self.recursive_size - 1 {
                for &coord in coords {
                    let current_kind = self.sub_maps[level][&coord].kind;
                    let max_kind = self.get_max_kind(level, coord, current_kind);

                    if max_kind == current_kind {
                        // TODO: Make no expect
                        for next_coord in coord
                            .to_coord()
                            .expect("Internal coord error")
                            .next_level_coords(
                                self.base,
                                self.map_size.to_size().expect("Internal size error"),
                            )
                        {
                            if self.sub_maps[level + 1].contains_key(&CS::new_coord(next_coord))
                                && self.empty_sub_map(level + 1, CS::new_coord(next_coord))
                                && self.sub_maps[level + 1][&CS::new_coord(next_coord)].kind
                                    == current_kind
                            {
                                self.sub_maps[level + 1].remove(&CS::new_coord(next_coord));
                            }
                        }
                    } else {
                        // TODO: Make no expect
                        for next_coord in coord
                            .to_coord()
                            .expect("Internal coord error")
                            .next_level_coords(
                                self.base,
                                self.map_size.to_size().expect("Internal size error"),
                            )
                        {
                            let sub_map = self.sub_maps[level + 1]
                                .entry(CS::new_coord(next_coord))
                                .or_insert(RecursiveSubMap {
                                    version: 0,
                                    edit_version: 0,
                                    kind: current_kind,
                                });
                            if sub_map.kind == max_kind
                                && self.empty_sub_map(level + 1, CS::new_coord(next_coord))
                            {
                                self.sub_maps[level + 1].remove(&CS::new_coord(next_coord));
                            }
                        }
                        self.sub_maps[level].get_mut(&coord).unwrap().kind = max_kind;
                        self.sub_maps[level].get_mut(&coord).unwrap().edit_version = self.sub_maps
                            [level]
                            .get_mut(&coord)
                            .unwrap()
                            .edit_version
                            .wrapping_add(1);
                    }
                }
            }
        }

        for (level, coords) in up_stack.iter().enumerate().rev() {
            for coord in coords {
                if let Some(sub_map) = self.sub_maps[level].get_mut(&coord) {
                    sub_map.version = sub_map.edit_version;
                }
            }
        }

        Ok(())
    }
}
