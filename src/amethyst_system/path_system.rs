use crate::{
    data_types::{CoordinateSystem, PassageMatrix, RecursiveKind},
    queue::EditQueue,
    recursive_map::{RecursiveMap, RecursivePath},
};
use amethyst::ecs::{prelude::*, Join};

pub struct PathRequest<CS, K>
where
    CS: CoordinateSystem,
    K: RecursiveKind,
{
    pub passage_matrix: PassageMatrix<K>,
    pub from: CS,
    pub to: CS,
}

impl<CS, K> Component for PathRequest<CS, K>
where
    CS: 'static + CoordinateSystem,
    K: 'static + RecursiveKind,
{
    type Storage = VecStorage<Self>;
}

#[derive(Default)]
pub struct MapSystem<CS, K>
where
    CS: CoordinateSystem,
    K: RecursiveKind,
{
    _cs: CS,
    _k: K,
}

impl<'a, CS, K> System<'a> for MapSystem<CS, K>
where
    CS: 'static + CoordinateSystem,
    K: 'static + RecursiveKind,
{
    type SystemData = (
        Entities<'a>,
        Write<'a, RecursiveMap<CS, K>>,
        Write<'a, EditQueue<CS, K>>,
        ReadStorage<'a, PathRequest<CS, K>>,
        WriteStorage<'a, RecursivePath<CS, K>>,
    );

    fn run(
        &mut self,
        (entities, mut rmap, mut edit_queue, path_requests, mut path_results): Self::SystemData,
    ) {
        edit_queue.commit(&mut rmap).expect("Edit Queue Error");

        for (entity, path) in (&*entities, &path_requests, !&path_results)
            .join()
            .map(|(entity, path_request, _)| {
                (
                    entity,
                    rmap.new_path(
                        &path_request.passage_matrix,
                        path_request.from,
                        path_request.to,
                    ),
                )
            })
            .collect::<Vec<_>>()
            .into_iter()
        {
            path_results.insert(entity, path).expect("New Path Error");
        }

        for path_result in (&mut path_results).join() {
            rmap.update_passage(&path_result.passage_matrix)
                .expect("Update Passage Error");
            rmap.refine_path(path_result).expect("Refine Path Error");
        }
    }
}
